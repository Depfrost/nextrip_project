﻿# Description

This is the repository of the project nextrip.
Nextrip is an application that can give to the user a customised circus travel.

# Working

2 branchs are here :
- One for the front (frontend)
- One for back (backend)

For your commits, use the following guideline :

[/*Front or Back*/][/*More specific Object*/] /*Description*/

DO NOT PUSH IN MASTER ! Only the 2 main branchs can be used to merge to master

# Contributors
The contributors are :

Nathan GOYAT (goyat_n)

Nicolas TANG (tang_n)

Louis TRAN (tran_8)

David GRONDIN (grondi_a)

# Supervisor

The supervisor is Cédric Eugéni.