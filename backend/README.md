# init

First, You have to create your database according to what is displaying in the settings "DATABASES"

# Run

Then you have to run3 steps :
- python manage.py makemigrations --settings server.settings
- python manage.py migrate
- python manage.py runserver

By default, it will run in port 8000.

# Issues

If you have some troubles with the migrations :

- Check that you have run your server psql
- Check if every migration has been done successfully. If not, run :
    - python manage.py makemigrations models /* Name of the migration */
    
# Neo4j

In order to synchronize neo4j with api, run :
- python manage.py install_labels

# Doc

In order to go to the swagger doc, run the server and go to :
localhost:8000/doc

# Deploy

In order to deploy the server :
- Go to backend directory
- Remove comment from settings
- merge request to master
