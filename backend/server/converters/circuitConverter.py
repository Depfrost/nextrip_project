def circuitConverterToJson(record):
    return dict(
        id=record.id,
        name=record.name,
        photo=record.photo
    )