def userConverterToJson(record, profil_to_json):
    return dict(
        id=record.id,
        username=record.username,
        first_name=record.first_name,
        last_name=record.last_name,
        email=record.email,
        photo=record.photo,
        category=profil_to_json,
        photoaccess=record.photoaccess,
        is_superuser=record.is_superuser,
        is_staff=record.is_staff,
        is_active=record.is_active,
        date_joined=record.date_joined,
    )