import json

def placesConverterToJson(record):
    to_json = []
    listOpen = json.loads(record.opening)
    for elem in listOpen:
        dayOpen = dict(
            close={} if 'close' not in elem else elem["close"],
            open=elem["open"]
        )
        to_json.append(dayOpen)
    return dict(
        id=record.id,
        name=record.name,
        address=record.address,
        longitude=record.longitude,
        latitude=record.latitude,
        city=record.city,
        description=record.description,
        opening=to_json,
        photo=record.photo
    )