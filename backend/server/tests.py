from server import models
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse as api_reverse
import json

from rest_framework_jwt.settings import api_settings
payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()


class NextripApiTestCase(APITestCase):

    superuser_obj = []
    user_obj1 = []
    user_obj2 = []
    parc_category = []
    place_obj1 = []
    place_obj2 = []
    circuit_obj = []

    def setUp(self):
        # Create superuser
        self.superuser_obj = User.objects.create_superuser('superuser1', 'superuser1@epita.fr', "password", category="{\"établissement\": 4, \"parc\": 2, \"église\": 6, \"lieu de culte\": 2, \"musée\": 6, \"monument\": 3.1, \"mairie\": 4, \"place\": 5}")
        self.superuser_obj.set_password("password")
        self.superuser_obj.save()
        # Create user1
        self.user_obj1 = User.objects.create_user('user1', 'user1@epita.fr', category="{\"établissement\": 3, \"parc\": 2, \"église\": 7, \"lieu de culte\": 2, \"musée\": 6, \"monument\": 3.1, \"mairie\": 4, \"place\": 5}")
        self.user_obj1.set_password("password")
        self.user_obj1.save()
        # Create user2
        self.user_obj2 = User.objects.create_user('user2', 'user2@epita.fr', category="{\"établissement\": 2, \"parc\": 2, \"église\": 6, \"lieu de culte\": 2, \"musée\": 6, \"monument\": 3.1, \"mairie\": 4, \"place\": 5}")
        self.user_obj2.set_password("password")
        self.user_obj2.save()
        # Create a category
        self.parc_category = models.Category(name="parc")
        self.parc_category.save()
        # Create all other categories
        models.Category(name="établissement").save()
        models.Category(name="église").save()
        models.Category(name="lieu de culte").save()
        models.Category(name="musée").save()
        models.Category(name="monument").save()
        models.Category(name="mairie").save()
        models.Category(name="place").save()
        opening1 = [
            {
               "close" : {
                  "day" : 0,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]
        opening2 = [
            {
               "close" : {
                  "day" : 1,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 0,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0900"
               }
            }
         ]
        openingtxt1 = json.dumps(opening1, ensure_ascii=False)
        openingtxt2 = json.dumps(opening2, ensure_ascii=False)
        # Create a place
        self.place_obj1 = models.Places(name="Arc de Triomphe", address="Place Charles de Gaulle, 75008 Paris, France",
                                        longitude="2.2950275", latitude="48.8737917", city="Paris", description="desc",
                                        place_id="ChIJjx37cOxv5kcRPWQuEW5ntdk", opening=openingtxt1, photo="")
        self.place_obj1.save()
        self.place_obj2 = models.Places(name="Catacombes de Paris", address="1 Avenue du Colonel Henri Rol-Tanguy, 75014 Paris, France",
                                        longitude="2.3324222", latitude="48.8338325", city="Paris", description="On appelle « catacombes » cet ossuaire parisien",
                                        place_id="ChIJdbbQwbZx5kcRs7Qu5nPw18g", opening=openingtxt2, photo="")
        self.place_obj2.save()
        # Create a category_place
        category_place_obj = models.Category_Places(place=self.place_obj1, category=self.parc_category)
        category_place_obj.save()
        # Create a circuit
        self.circuit_obj = models.Circuit(name="Test circuit", city="Paris")
        self.circuit_obj.save()
        # Create a circuit_place
        circuit_place_obj = models.Circuit_Places(circuit=self.circuit_obj, place=self.place_obj1)
        circuit_place_obj.save()
        # Create a circuit_user
        circuit_user_obj = models.Circuit_Users(circuit=self.circuit_obj, user=self.user_obj1)
        circuit_user_obj.save()

    def login_user1(self):
        # Login the user1
        payload = payload_handler(self.user_obj1)
        token_response = encode_handler(payload)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token_response)

    def login_user2(self):
        # Login the user2
        payload = payload_handler(self.user_obj2)
        token_response = encode_handler(payload)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token_response)

    def login_superuser(self):
        # Login the superuser
        payload = payload_handler(self.superuser_obj)
        token_response = encode_handler(payload)
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token_response)

    def logout(self):
        self.client.credentials()

    # Test number of user in DB
    def test_nb_user(self):
        user_count = User.objects.count()
        self.assertEqual(user_count, 3)

    # Test that we can get a valid token
    def test_get_token_credentials(self):
        invalid_data = {
            "username": "superuser1",
            "password": "wrongpassword"
        }
        valid_data = {
            "username": "superuser1",
            "password": "password"
        }
        url = api_reverse("api-login")
        # Test that GET isn't allowed
        response = self.client.get(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test with wrong credentials
        response = self.client.post(url, invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test with correct credentials
        response = self.client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Check that the token isn't None
        token = response.data.get("token")
        self.assertIsNotNone(token)

    def test_get_user(self):
        data = {}
        url = api_reverse("user-get")
        # Test with unidentified user
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test with user1
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get("username"), "user1")
        self.logout()

    # Test that we can get the list of users
    def test_get_user_list(self):
        data = {}
        url = api_reverse("user-listcreate")
        # Test with unidentified user
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # Test with simple user
        self.login_user1()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Test with superuser
        self.login_superuser()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.logout()

    # Test user creation
    def test_create_user(self):
        invalid_data_username = {
            "username": "user1",
            "email": "user3@epita.fr",
            "password": "password",
        }
        invalid_data_email = {
            "username": "user3",
            "email": "user1@epita.fr",
            "password": "password",
        }
        invalid_data_username_missing = {
            "email": "user1@epita.fr",
            "password": "password"
        }
        invalid_data_password_missing = {
            "username": "user3",
            "email": "user1@epita.fr",
        }
        valid_data = {
            "username": "user3",
            "last_name": "user3",
            "first_name": "user3",
            "email": "user3@epita.fr",
            "password": "password",
            "photo": "https://www.gravatar.com/avatar/1cfac2812c7b54b012e86e13ece2ba74?s=32&d=identicon&r=PG&f=1",
            "category": "[{\"id\": 1, \"name\": \"parc\", \"value\": 1}]",
            "photoaccess": "True",
        }
        url = api_reverse("user-listcreate")
        # Test with already used username
        response = self.client.post(url, invalid_data_username, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test with already used email
        response = self.client.post(url, invalid_data_email, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test with username missing
        response = self.client.post(url, invalid_data_username_missing, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test with password missing
        response = self.client.post(url, invalid_data_password_missing, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Create user with valid data
        response = self.client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Delete user
        self.login_superuser()
        url = response.data.get("url")
        response = self.client.delete(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.logout()

    # Test retrieving a specific user
    def test_get_user(self):
        data = {}
        self.logout()
        # Test without being authenticated
        url = self.superuser_obj.get_api_url()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # Authenticated with another user
        self.login_user1()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Authenticated with same user
        url = self.user_obj1.get_api_url()
        self.login_user1()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Authenticated with superuser
        self.login_superuser()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.logout()

    # Test the deletion of user
    def test_delete_user(self):
        # Create a new user
        data = {
            "username": "user3",
            "password": "password"
        }
        url_creation = api_reverse("user-listcreate")
        response = self.client.post(url_creation, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test without being authenticated
        self.logout()
        url_deletion = response.data.get("url")
        response = self.client.delete(url_deletion, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # Authenticated with another user
        self.login_user1()
        response = self.client.delete(url_deletion, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Authenticated with superuser
        self.login_superuser()
        response = self.client.delete(url_deletion, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # Recreate the user
        response = self.client.post(url_creation, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Authenticated with the same user
        url_deletion = response.data.get("url")
        url_login = api_reverse("api-login")
        response = self.client.post(url_login, data, format='json')
        token = response.data.get("token")
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        response = self.client.delete(url_deletion, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.logout()

    def test_update_item(self):
        new_valid_data = {
            "username": "user3",
            "first_name": "user4",
            "last_name": "user4",
            "email": "user4@epita.fr",
            "password": "password",
            "photo": "https://www.gravatar.com/avatar/1cfac2812c7b54b012e86e13ece2ba74?s=32&d=identicon&r=PG&f=1",
            "category": "[{\"id\": 9, \"name\": \"mairie\", \"value\": 4}]",
            "photoaccess": "False",
        }
        new_invalid_data_username = {
            "username": "user1",
            "first_name": "user4",
            "last_name": "user4",
            "email": "user4@epita.fr",
            "password": "password",
            "photo": "https://www.gravatar.com/avatar/1cfac2812c7b54b012e86e13ece2ba74?s=32&d=identicon&r=PG&f=1",
            "category": "[{\"id\": 9, \"name\": \"mairie\", \"value\": 4}]",
            "photoaccess": "False",
            "is_superuser": "True"
        }
        new_invalid_data_email = {
            "username": "user4",
            "first_name": "user4",
            "last_name": "user4",
            "email": "user1@epita.fr",
            "password": "password",
            "photo": "https://www.gravatar.com/avatar/1cfac2812c7b54b012e86e13ece2ba74?s=32&d=identicon&r=PG&f=1",
            "category": "[{\"id\": 9, \"name\": \"mairie\", \"value\": 4}]",
            "photoaccess": "False",
        }
        # Create a new user
        data = {
            "username": "user3",
            "first_name": "user3",
            "last_name": "user3",
            "email": "user3@epita.fr",
            "password": "password",
            "photo": "https://www.gravatar.com/avatar/1cfac2812c7b54b012e86e13ece2ba74?s=32&d=identicon&r=PG&f=1",
            "category": "[{\"id\": 9, \"name\": \"mairie\", \"value\": 4}]",
            "photoaccess": "True",
        }
        url_creation = api_reverse("user-listcreate")
        response = self.client.post(url_creation, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        url_user = response.data.get("url")
        # Update while not authenticated
        self.logout()
        response = self.client.put(url_user, new_valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # Update with another user
        self.login_user1()
        response = self.client.put(url_user, new_valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Update with superuser
        self.login_superuser()
        response = self.client.put(url_user, new_valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update with already used username
        response = self.client.put(url_user, new_invalid_data_username, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Update with already used email
        response = self.client.put(url_user, new_invalid_data_email, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Update with good user
        url_login = api_reverse("api-login")
        response = self.client.post(url_login, data, format='json')
        token = response.data.get("token")
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        response = self.client.put(url_user, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Delete the user
        self.login_superuser()
        response = self.client.delete(url_user, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.logout()

    def test_server_health(self):
        data = {}
        url = api_reverse("server-health")
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get the server status
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        server_status = response.json().get("status")
        self.assertEqual(server_status, 'alive')

    def test_categories(self):
        data = {}
        url = api_reverse("categories")
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get categories
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content
        self.logout()

    def test_category_by_id(self):
        data = {}
        url = api_reverse("category-by-id", args=[1])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get categories
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_category_by_place(self):
        data = {}
        url = api_reverse("category-by-place", args=[1])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get categories
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_post_circuit(self):
        valid_data = {
            "name": "MyCircuit",
            "city": "Paris",
            "user_id": 2,
            "list": "[{\"Place_id\": 1},{\"Place_id\": 2}]"
        }
        good_data_name = {
            "city": "Paris",
            "user_id": 2,
            "list": "[{\"Place_id\": 1},{\"Place_id\": 2}]"
        }
        wrong_data_city = {
            "name": "MyCircuit",
            "user_id": 2,
            "list": "[{\"Place_id\": 1},{\"Place_id\": 2}]"
        }
        wrong_data_userid = {
            "name": "MyCircuit",
            "city": "Paris",
            "user_id": 10,
            "list": "[{\"Place_id\": 1},{\"Place_id\": 2}]"
        }
        wrong_data_list_id = {
            "name": "MyCircuit",
            "city": "Paris",
            "user_id": 2,
            "list": "[{\"Place_id\": 15},{\"Place_id\": 28}]"
        }
        url = api_reverse("circuit-create")
        # Test with unauthenticated user
        self.logout()
        response = self.client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        # Test with superuser
        self.login_superuser()
        response = self.client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test with unauthorized user
        self.login_user2()
        response = self.client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.login_user1()
        # Test that GET isn't allowed
        response = self.client.get(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test missing name
        response = self.client.post(url, good_data_name, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test missing city (should work)
        response = self.client.post(url, wrong_data_city, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test bad user_id
        response = self.client.post(url, wrong_data_userid, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Test invalid json list
        response = self.client.post(url, wrong_data_list_id, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Create the circuit with valid data
        response = self.client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Need to delete parts of the circuit in all tables
        self.logout()

    def test_get_circuit_details(self):
        data = {}
        url = api_reverse("circuit-by-id", args=[1])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get circuit
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_get_circuit_by_place_userid(self):
        data = {}
        url = api_reverse("circuit-by-place-userid", args=['Paris', 2])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get circuit
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Check with another user
        self.login_user2()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Check with a superuser
        self.login_superuser()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_get_circuit_by_userid(self):
        data = {}
        url = api_reverse("circuit-by-userid", args=[2])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get circuit
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Check with another user
        self.login_user2()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Check with a superuser
        self.login_superuser()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_get_place_by_id(self):
        data = {}
        url = api_reverse("place-by-id", args=[1])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get place
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_get_place_by_city_categoryid(self):
        data = {}
        url = api_reverse("place-by-city-categoryid", args=["Paris", 1])
        # Test while unauthenticated
        self.logout()
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that POST isn't allowed
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Get place
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # May need to check the content and invalid arg
        self.logout()

    def test_post_mark(self):
        data = {
            "place": 1,
            "user": 2,
            "marks": 4
        }
        data_update = {
            "place": 1,
            "user": 2,
            "marks": 2
        }
        invalid_mark = {
            "place": 1,
            "user": 2,
            "marks": -3
        }
        url = api_reverse('rate-place')
        # Test while unauthenticated
        self.logout()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that GET isn't allowed
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Test post valid mark from invalid account
        self.login_user2()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Test post invalid mark
        self.login_user1()
        response = self.client.post(url, invalid_mark, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test post new valid mark
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test update existing mark
        response = self.client.post(url, data_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        mark_count = models.Places_Marks.objects.filter(place=1, user=2).count()
        self.assertEqual(mark_count, 1)
        # May need to check the content and invalid arg
        self.logout()

    def test_rate_circuit(self):
        data = {
            "circuit": 1,
            "user": 2,
            "marks": 4
        }
        data_update = {
            "circuit": 1,
            "user": 2,
            "marks": 2
        }
        invalid_mark = {
            "circuit": 1,
            "user": 2,
            "marks": -3
        }
        url = api_reverse('rate-circuit')
        # Test while unauthenticated
        self.logout()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.login_user1()
        # Test that GET isn't allowed
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PUT isn't allowed
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that PATCH isn't allowed
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Test that DELETE isn't allowed
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Test post valid mark from invalid account
        self.login_user2()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Test post invalid mark
        self.login_user1()
        response = self.client.post(url, invalid_mark, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test post new valid mark
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test update existing mark
        response = self.client.post(url, data_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        mark_count = models.Circuits_Marks.objects.filter(user=2, circuit=1).count()
        self.assertEqual(mark_count, 1)
        # May need to check the content and invalid arg
        self.logout()
