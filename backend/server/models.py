from django.db import models
from django.contrib.auth.models import AbstractUser
from rest_framework.reverse import reverse as api_reverse
from neomodel import StructuredNode, StringProperty, DateProperty, IntegerProperty, config


class User(AbstractUser):
    photo = models.URLField(default='https://www.drupal.org/files/profile_default.png')
    category = models.TextField(default='{}')
    photoaccess = models.BooleanField(default=False)

    def get_api_url(self, request=None):
        return api_reverse("user-rud", kwargs={'pk': self.pk}, request=request)


class Places(models.Model):
    name = models.TextField(default='')
    address = models.TextField(default='')
    longitude = models.FloatField(default='')
    latitude = models.FloatField(default='')
    city = models.TextField(default='')
    description = models.TextField(default='', null=True)
    place_id = models.TextField(default='')
    opening = models.TextField(default='')
    photo = models.TextField(default='')
    duration = models.FloatField(default=1)
    rating = models.FloatField(default=-1)


class Category(models.Model):
    name = models.TextField(default='')


class Category_Places(models.Model):
    place = models.ForeignKey('Places', unique=False, on_delete=models.CASCADE)
    category = models.ForeignKey('Category', unique=False, on_delete=models.CASCADE)


class Circuit(models.Model):
    name = models.CharField(max_length=50, default='New Circuit')
    city = models.TextField(default='')
    photo = models.TextField(default='')
    rating = models.FloatField(default=-1)


class Circuit_Places(models.Model):
    circuit = models.ForeignKey('Circuit', unique=False, on_delete=models.CASCADE)
    place = models.ForeignKey('Places', unique=False, on_delete=models.CASCADE)


class Circuit_Users(models.Model):
    circuit = models.ForeignKey('Circuit', unique=False, on_delete=models.CASCADE)
    user = models.ForeignKey('User', unique=False, on_delete=models.CASCADE)


class Circuit_Recommendation(StructuredNode):
    cimetiere = IntegerProperty()
    id = IntegerProperty()
    lieu_de_culte = IntegerProperty()
    mairie = IntegerProperty()
    monument = IntegerProperty()
    musee = IntegerProperty()
    parc = IntegerProperty()
    place = IntegerProperty()
    supermarche = IntegerProperty()
    eglise = IntegerProperty()
    etablissement = IntegerProperty()


class Profil(StructuredNode):
    city = StringProperty()
    id = IntegerProperty()
    name = StringProperty()


class Places_Marks(models.Model):
    place = models.ForeignKey('Places', unique=False, on_delete=models.CASCADE)
    user = models.ForeignKey('User', unique=False, on_delete=models.CASCADE)
    marks = models.FloatField(default=-1)


class Circuits_Marks(models.Model):
    circuit = models.ForeignKey('Circuit', unique=False, on_delete=models.CASCADE)
    user = models.ForeignKey('User', unique=False, on_delete=models.CASCADE)
    marks = models.FloatField(default=-1)
