import sys

import requests


def getTimeBeforeArrivedPlace(origin, destination):
    mode = "walking"
    key = "AIzaSyD8PiiRXMKEc28UfDPdX0hZCy9glYGaNfM"
    payload = {'origins': origin, 'destinations': destination, 'mode': mode, 'key': key}
    if 'test' in sys.argv:
        json_resp = {'destination_addresses': ['2 Place Charles de Gaulle, 75008 Paris, France'],
                     'origin_addresses': ['2 Place Charles de Gaulle, 75008 Paris, France'], 'rows': [{'elements': [
                {'distance': {'text': '1 m', 'value': 0}, 'duration': {'text': '1 min', 'value': 0}, 'status': 'OK'}]}],
                     'status': 'OK'}
    else:
        r = requests.get("https://maps.googleapis.com/maps/api/distancematrix/json", params=payload)
        if r.status_code != 200:
            raise Exception
        json_resp = r.json()
    return json_resp