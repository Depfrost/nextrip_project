import requests


def getDailyWeatherHumidity():
    weatherKey = "68df48dae0745d1217f20ec70fd8fbef"
    weatherBaseUrl = "https://api.openweathermap.org/data/2.5/weather?q="
    weatherCity = "Paris"
    payload = {'q': weatherCity, 'lang': 'fr', 'appid': weatherKey}
    weather = requests.get(weatherBaseUrl + weatherCity, params=payload)
    return weather.json()['main']['humidity']