INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(0, 'Arc de Triomphe', 'Place Charles de Gaulle, 75008 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJjx37cOxv5kcRPWQuEW5ntdk',  2.2950275, 48.8737917, 'Paris', 'Symbole et monument incontournable de la capitale, la Tour Eiffel culmine à 325 m de hauteur pour un poids total de 10 100 tonnes, dont 7 300 tonnes pour la charpente métallique. Réalisée en 2 ans, 2 mois et 5 jours, elle fut érigée à l’occasion de l’Exposition Universelle de 1889.', 'https://lh5.googleusercontent.com/p/AF1QipO_2V6D9s-knHS2QPIhYNnbCgV6q2BoHVt0Ug5D=w408-h270-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(8, 'Conciergerie', '2 Boulevard du Palais, 75001 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0930"
               }
            }
         ]', 'ChIJR3122B9u5kcRajG2bEZLypA',  2.3454953, 48.8560114, 'Paris', 'La Conciergerie, important vestige du palais des Capétiens, offre un remarquable témoignage sur l’architecture civile du XIVe siècle avec la salle des Gens d’Armes, la salle des Gardes et les cuisines. La quasi-totalité du niveau bas du palais fut transformée en prison au XVe siècle. On peut y visiter les cachots et, notamment pour Marie-Antoinette, la chapelle expiatoire dédiée à sa mémoire et qui fut l’emplacement de sa prison.', 'https://lh5.googleusercontent.com/p/AF1QipMZoEal24KDXw-Q7hAh_Nu-saW75pfaHUxjqtzC=w408-h270-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(9, 'Tour Eiffel', 'Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France', '[
            {
               "close" : {
                  "day" : 1,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 0,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0900"
               }
            }
         ]', 'ChIJLU7jZClu5kcR4PcOOO6p3I0',  2.2944813, 48.85837009999999, 'Paris', 'Symbole et monument incontournable de la capitale, la Tour Eiffel culmine à 325 m de hauteur pour un poids total de 10 100 tonnes, dont 7 300 tonnes pour la charpente métallique. Réalisée en 2 ans, 2 mois et 5 jours, elle fut érigée à l’occasion de l’Exposition Universelle de 1889.', 'https://lh5.googleusercontent.com/p/AF1QipPthl0Czel9ci6ia1vsBkAbvw6oxvrS2duTTTPA=w408-h272-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(10, 'Esplanade des Invalides', '1 Rue Fabert, 75007 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJv-rX7Ndv5kcR8HH74UZfYtQ',  2.3131693, 48.8599152, 'Paris', 'Reliant le pont Alexandre III à l’Hôtel des Invalides, cette grande esplanade ouverte, bordée d’un mail de tilleuls, offre de magnifiques perspectives sur certains des plus beaux monuments de Paris.', 'https://lh5.googleusercontent.com/p/AF1QipMGkQ4IQOfkV9ISV2Vbo1bYFoURK8lRYBjyj8WP=w408-h306-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(11, 'Fontaine Saint-Michel', 'Place Saint-André des Arts, 75006 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJvSjKKN5x5kcRHc9taVU9MZY',  2.3438185, 48.8532277, 'Paris', 'La fontaine Saint Michel est située dans l’angle entre le boulevard Saint-Michel et la Place Saint-André-des-Arts. Cette fontaine, représentant l’archange Michel terrassant le Diable, évoque la lutte du Bien contre le Mal. Elle a été commandée par Haussmann sous Napoléon III.', 'https://lh5.googleusercontent.com/p/AF1QipOK9obzoYJL2uF5AvZxF8tOBzpMJfSdUaL37nPX=w408-h306-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(7, 'Musée du Louvres', 'Rue de Rivoli', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "2200"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2200"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0900"
               }
            }
         ]', 'ChIJD3uTd9hx5kcR1IQvGfr8dbk', 2.337644, 48.8606111, 'Paris', ' Il rassemble des œuvres de l’art occidental du Moyen Âge à 1848, des civilisations antiques orientales, égyptiennes, grecques, étrusques, romaines, des arts graphiques et des arts de l’Islam. De salle en salle, l’ancien palais royal dévoile ses chefs-d’oeuvre : la Joconde, Le Radeau de la Méduse, la Vénus de Milo, La Victoire de Samothrace… Au total, quelque 35 000 œuvres ! En huit siècles d’existence, le Louvre a été marqué par de nombreux courants architecturaux, de la forteresse médiévale du XIIe siècle à la pyramide de verre de Pei (1989). Dernier ajout : les architectes Mario Bellini et Rudy Ricciotti ont signé le nouvel écrin accueillant les Arts de l’Islam, une verrière ondulante recouvrant la cour Visconti et inondant de lumière les 2 800 m² du nouveau département. La visite du musée est particulièrement agréable lors des nocturnes : moins fréquenté, le Louvre offre une atmosphère différente et des vues imprenables sur la Pyramide de Pei, la cour Carrée et la Seine.', 'https://lh5.googleusercontent.com/p/AF1QipMjUFT4Qo3D0jbb3VQxV7s_CVlpDQaDchQJ1mpk=w408-h272-k-no', 4);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(12, 'Place de l''Alma', 'Place de l''Alma', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'Eh5QbGFjZSBkZSBsJ0FsbWEsIFBhcmlzLCBGcmFuY2UiSCpGChQKEgmDaemX3W_mRxGoDxIR1Ce56BIUChIJD7fiBh9u5kcRYJSMaMOCCwQaGAoKDagUIB0V5w5fARIKDZo4IB0VTkxfAQ', 2.3013862, 48.8646975, 'Paris', 'La Place de l''Alma s''étend sur le 8ème et le 16ème arrondissement de Paris, sur une longueur d''environ 110m et se situe à la rencontre des avenues de New-York, du Président-Wilson, George-V, Montaigne et du cours Albert Ier. Elle est bien installée au-dessus du tunnel où la princesse Lady Diana a trouvé la mort en 1997 lors d''un accident de voiture. La place est aussi connue du monde entier par sa Flamme de la Liberté, une réplique de la flamme de la Statue de la Liberté.', 'https://geo1.ggpht.com/maps/photothumb/fd/v1?bpb=ChAKDnNlYXJjaC5UQUNUSUxFEiAKEgmDaemX3W_mRxGoDxIR1Ce56CoKDQAAAAAVAAAAABoGCMgBEJgD&gl=FR', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(13, 'Cathédrale Notre-Dame de Paris', '6 Parvis Notre-Dame - Pl. Jean-Paul II', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJATr1n-Fx5kcRjQb6q6cdQDY', 2.3499021, 48.85296820000001, 'Paris', 'La Cathédrale Notre-Dame de Paris est sans doute la plus impressionnante des Cathédrales de France, de par sa construction gigantesque qui a duré environ deux siècles, commençant en 1160 sous l''ordre de Maurice de Sully. D''une surface de 5500m², avec 130m de longueur, 48m de largeur et 35m de hauteur, la Cathédrale impressionne par sa taille et par son architecture magnifiquement détaillé. On peut y accéder par une façade majestueuse présentant 3 portails : le portail de Sainte-Anne, du jugement dernier et de la Vierge. Sur les deux tours, on peut découvrir la cloche de 13 tonnes d''un côté, et de l''autre un escalier qui compte 387 marches, sur la hauteur duquel on peut admirer une vue panoramique de Paris. La beauté de la cathédrale a été mise en valeur durant sa 850ème anniversaire en 2013, à travers de nombreuses manifestations culturelles et musicales.', 'https://lh5.googleusercontent.com/p/AF1QipOE7KpudPnvytpqIpOB73y4oSh_8vCvaUEPt96o=w408-h272-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(14, 'Hôtel de Ville', 'Place de l''Hôtel de Ville, 75004 Paris, France', '[
            {
               "close" : {
                  "day" : 1,
                  "time" : "1930"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0800"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1930"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0800"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1930"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0800"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1930"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0800"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1930"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0800"
               }
            }
         ]', 'ChIJi1uPs_1x5kcRbh8M8XJSNMA',  2.3524135, 48.8564826, 'Paris', 'Depuis 1357, l’Hôtel de Ville de Paris est le siège de la municipalité parisienne. Le bâtiment actuel, de style néo renaissance, a été reconstruit par les architectes Théodore Ballu et Edouard Deperthes à l''emplacement de l''ancien Hôtel de Ville, incendié pendant le Commune de Paris en 1871. Il est possible de visiter la Mairie, lieu de pouvoir et de prestige. Des visites guidées, sur réservation, sont organisées par le Service du Protocole.', 'https://lh5.googleusercontent.com/p/AF1QipP7odSi38rMNpLuXTb4qj1k7erzfIRuLF-XK201=w408-h272-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(17, 'Le Palais Royal', '8 Rue de Montpensier, 75001 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "2300"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0700"
               }
            }
         ]', 'ChIJR3122B9u5kcR9RZ_OWxSy80',  2.3371261, 48.8637569, 'Paris', 'Créés par le cardinal de Richelieu en 1633, le Palais Royal et ses jardins, à deux pas du musée du Louvre, accueillaient les familles royales jusqu’à la construction de Versailles. Prestigieux et paisibles, les jardins sont enserrés dans un superbe ensemble architectural tourné vers l’avenir avec ses sculptures contemporaines de Buren et de Bury. Commandée à l’artiste Daniel Buren, les colonnes de Buren sont situées dans la cour, près du jardin et du Ministère de la Culture. Les 260 colonnes octogonales rayées de noir et de blanc méritent bien un détour et sont devenues l’un des symboles de Paris.', 'https://lh6.googleusercontent.com/proxy/hczBB-n74mw4W7qHHZXB-Ej9cohCxYYfw4H5rfoOM-HEqdEUIj_luE2jXZGCSY_BYLkJqsrCbznpDpwKMGHoZXhnMba1WgJbWnX7gI2U7sgnTd5ax8sGcYjPiFfoSEMVc7WZ7g96ast3LAKzdUc8rD6rAtTdOH0=w408-h272-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(19, 'Les Invalides', 'Place des Invalides, 75007 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1700"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJUzCPuddv5kcRasGAnEUUWkU',  2.312634, 48.8550601, 'Paris', 'En 1670, aucune fondation n''existait pour abriter les soldats invalides sans ressources ayant combattu pour la France. Louis XIV, sensible au sort des militaires qui l''ont servi lors de ses nombreuses campagnes, décide alors de construire l''hôtel royal des Invalides. Construit de 1671 à 1676 par Libéral Bruant, puis par Jules Hardouin-Mansart et Robert de Cotte, il est l''un des monuments les plus prestigieux de Paris. Aujourd''hui affecté au ministère de la Défense mais occupé également par de nombeux organismes dépendant d''autres ministères, l''hôtel national des Invalides conserve toujours sa fonction première d''hôpital-hospice pour les grands invalides, combattants blessés ou mutilés de fait de guerre. Outre le musée de l''Armée, il abrite le musée des Plans-Reliefs et le musée de l''Ordre de la Libération ainsi que les deux églises : l''Eglise du Dôme avec le tombeau de Napoléon I, exécuté sur les dessins de Visconti en 1843, et l''Eglise Saint-Louis des Invalides. Au cours de la deuxième moitié du XXè siècle, l''ensemble du site de l''hôtel national des Invalides a été dégagé à la vue du public par la destruction de petits bâtiments postérieurs et la création d''un fossé périphérique. En 1981, un vaste programme de restauration a été entrepris à l''hôtel national des Invalides sous l''implulsion d''une commission interministérielle co-présidée par les ministères de la Défense et de la Culture pour redonner sa splendeur passée à cet ensemble exceptionnel.', 'https://lh5.googleusercontent.com/p/AF1QipPQvtedsCB4NE7SRgwggdWy_oyoRB0I6v2JrDjG=w408-h200-k-no-pi-24.114584-ya65.67711-ro-0-fo100', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(18, 'L''Église de la Madeleine', 'Place de la Madeleine, 75008 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0930"
               }
            }
         ]', 'ChIJ7xwB9TJu5kcRtsJIlPxT918',  2.3245502, 48.8700435, 'Paris', 'L’église de la Madeleine est située entre la place de la Concorde et l’Opéra Garnier, dans le Paris haussmannien. Sa construction débute en 1764 et se termine en 1842. Son aspect, atypique pour un édifice religieux, a la forme d’un temple grec sans croix ni clocher. Le souhait de Napoléon était d’en faire un panthéon à la gloire de ses armées. Avant d’entrer par les deux portes monumentales en bronze, on admire les colonnes corinthiennes qui entourent l’édifice. À l’intérieur : sculptures, peintures et la célèbre mosaïque (composée par Charles-Joseph Lameire) de style néo-byzantin. Le magnifique grand orgue de l’église est signé Aristide Cavaillé-Coll. Tout au long de l’année, de jour comme de nuit, l’église programme des concerts de musique classique de grande qualité.', 'https://lh5.googleusercontent.com/p/AF1QipNMKKjEMOZg5C9e607k0gQsrYzvQL9pTIitd-TK=w408-h306-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(23, 'Obélisque de Louxor', 'Place de la Concorde, 75008 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJC_jkvdJv5kcRNX4NW3iuID8',  2.3211193, 48.86547849999999, 'Paris', 'C''est Méhémet Ali, vice-roi d''Égypte, en signe de bonne entente qui, avec l''accord du baron Taylor puis de Jean-François Champollion, offre à Charles X et la France au début de 1830 les deux obélisques érigés devant le temple de Louxor, mais seul celui de droite (en regardant le temple) est abattu et transporté vers la France.', 'https://lh5.googleusercontent.com/proxy/i_xaZN9yNVyfv2qp7QoBn7m7-iFEeLj_zFr1OpYAgQuV-W9c5t5Te6eHBeErjS_CjcH5dQw1-gz9Q91TZiRs4O4Zx-wj48n0UxVFIN7HJiuKgDmUFrNTZgKNggKJA20AlB4CdfIeDC8MFGaUkFgl7esIsyspkA=w408-h306-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(27, 'Musée d''Orsay', '1 Rue de la Légion d''Honneur, 75007 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2145"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0930"
               }
            }
         ]', 'ChIJG5Qwtitu5kcR2CNEsYy9cdA',  2.3265614, 48.8599614, 'Paris', 'Connu dans le monde entier pour sa riche collection d''art impressionniste, le musée d''Orsay est aussi le musée de toute la création artistique du monde occidental de 1848 à 1914. Ses collections représentent toutes les formes d''expression, de la peinture à l''architecture, en passant par la sculpture, les arts décoratifs, la photographie. Vous ne manquerez pas non plus d''être ébloui par la beauté du lieu : une gare aux allures de palais inauguré pour l''exposition universelle de 1900.', 'https://lh5.googleusercontent.com/p/AF1QipPZmljCU6v6PNW-8Gs9H_Skw8az8JPwmh8_C-qs=w408-h271-k-no', 3);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(28, 'Musée d''Art et d''Histoire du Judaïsme', 'Hôtel de Saint-Aignan, 71 Rue du Temple, 75003 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJydDIqRxu5kcRqB6W-MQwcYU',  2.3556112, 48.8610102, 'Paris', 'Situé dans le quartier historique du Marais, dans l’un des plus beaux hôtels particuliers de Paris, le Musée d’art et d’histoire du Judaïsme retrace l’évolution des communautés juives à travers leur patrimoine culturel et leurs traditions. Il accorde une place privilégiée à l’histoire des juifs en France, tout en évoquant les communautés d’Europe et d’Afrique du Nord, qui ont contribué à former la physionomie du judaïsme français actuel. Outre des objets d’art cultuel, des textiles et des manuscrits, le musée présente des documents uniques sur les courants intellectuels, l’art et l’histoire - dont des archives de l’affaire Dreyfus. Chagall, Modigliani, Soutine, Kikoïne illustrent, parmi d’autres artistes, la présence juive dans l’art du XXe siècle. Centre de documentation en accès libre (bibliothèque, photothèque, vidéothèque) ; auditorium de 198 places.', 'https://lh5.googleusercontent.com/p/AF1QipPit_Flj-AIa9GzHFqxEcVBem6a6zBYHcG2cg4Z=w408-h272-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(29, 'Musée de l''Orangerie', 'Jardin Tuileries, 75001 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0900"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0900"
               }
            }
         ]', 'ChIJo6qq6i5u5kcRCpYBp4rQP9w',  2.3226724, 48.8637884, 'Paris', 'Dans deux salles aux dimensions impressionnantes, le musée de l’Orangerie abrite une série de grands formats des Nymphéas, chef-d’œuvre offert par Claude Monet lui-même à la France en 1922. Le sous-sol accueille la collection Jean Walter et Paul Guillaume, consacrée à de grands noms du XXe siècle : Renoir, Cézanne, Matisse, Picasso, Soutine…', 'https://lh5.googleusercontent.com/p/AF1QipM14cGjQqiWuYz2WCRRgZgLeq3ZeiM7ClR1APFG=w408-h272-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(31, 'Palais de Tokyo', '13 Avenue du Président Wilson, 75116 Paris, France', '[
            {
               "close" : {
                  "day" : 1,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1200"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1200"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1200"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1200"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1200"
               }
            },
            {
               "close" : {
                  "day" : 0,
                  "time" : "0000"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1200"
               }
            }
         ]', 'ChIJC0I3-eZv5kcRHaMcXnrRLsk',  2.2966748, 48.8645866, 'Paris', 'Le Palais de Tokyo occupe un bâtiment monumental construit en 1937 à l’occasion de l’Exposition internationale "Arts et Techniques dans la Vie Moderne". Il est situé dans le 16e arrondissement, à deux pas du Trocadéro et de la tour Eiffel. L’aile ouest du palais abrite l’un des plus grands centres européens de création et d’art contemporain. Designé par les architectes Anne Lacaton et Jean-Philippe Vasall, le site propose au public des expositions, des rencontres, des projections, des concerts et des performances, dans un espace de 22 000m² répartis sur quatre étages. Des animations et des ateliers pour enfants sont également disponibles. Les équipements sont nombreux : deux restaurants, deux jardins et une librairie. L’aile est du Palais de Tokyo est occupée par le musée d’art moderne de la Ville de Paris.', 'https://lh5.googleusercontent.com/p/AF1QipMTh3I_YP3gBN49HmTorWKOdexHeJIrneIW0x0I=w408-h271-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(33, 'Panthéon', 'Place du Panthéon, 75005 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1830"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJc8mX0udx5kcRWKcjTwDr5QA', 2.3464138, 48.8462218, 'Paris', 'Sa silhouette et son dôme se détachent sur la montagne Sainte-Geneviève, colline historique du Quartier latin. Cette ancienne église dédiée à la sainte patronne de Paris abrite dans sa crypte, depuis la Révolution française, une nécropole de grands personnages de la république - dont Victor Hugo, Marie Curie et Alexandre Dumas. D’avril à octobre, on apprécie la vue sur Paris depuis la colonnade du dôme.', 'https://lh5.googleusercontent.com/p/AF1QipNmYn_J1lE13fz129lcXNcK2GVeRGAokGn3zp-Q=w408-h271-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(37, 'Petit Palais', 'Avenue Winston Churchill, 75008 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJSUOPztFv5kcRnEbSPYG-9fM',  2.3145896, 48.8660479, 'Paris', 'Le Petit Palais est un joyau architectural 1900, situé sur l’avenue des Champs-Élysées. Le bâtiment, construit à l’occasion de l’Exposition Universelle, comme le Grand Palais qui lui fait face, abrite le musée des Beaux-Arts de la Ville de Paris. Les 1 300 œuvres du musée (sculptures, peintures, tapisseries, objets d’arts, icônes) proposent un large panorama artistique. Les collections antiques et médiévales côtoient les œuvres de la Renaissance française et italienne ou la peinture flamande et hollandaise. Le visiteur découvre notamment la collection de tableaux français d’artistes majeurs du XIXe siècle : Delacroix, Monet, Sisley, Renoir, Toulouse-Lautrec, Courbet… Un jardin intérieur, où il fait bon flâner, accentue le charme des lieux.', 'https://lh5.googleusercontent.com/p/AF1QipO5_heV-P9EzSWfYpOHqh9HL4kScUs5p8La2KVS=w408-h271-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(39, 'Place Dauphine', '75001 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJBaiyid9x5kcRQ7sNQt4yTqU',  2.3425083, 48.8565422, 'Paris', 'Yves Montant et Simone Signoret l''ont immortalisé en logeant au n°15...Située à l''ouest de l''Île de la Cité, la place Dauphine est la seconde place royale parisienne du XVIIe siècle. Riche d''un passé historique et architecturale, la place a, malgré tout, gardé sa quiétude intacte, du fait de l''épaisseur des murs des bâtiments qui l''encerclent. Cet endroit est devenu le repaire des galeries d''art, des cafés et des amoureux...', 'https://lh5.googleusercontent.com/p/AF1QipMlOdVPS5dS02_x4SWm3m35mq6x1515JrlLH74F=w408-h306-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(38, 'Musée Picasso National', '5 Rue de Thorigny, 75003 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1030"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1030"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1030"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1030"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0930"
               }
            }
         ]', 'ChIJS2t0QgFu5kcR92388prakcE',  2.362285, 48.8598775, 'Paris', 'La collection du Musée national Picasso-Paris compte plus de 5 000 œuvres et plusieurs dizaines de milliers de pièces d’archives. Par sa qualité, son ampleur comme par la diversité des domaines artistiques représentés, elle est la seule collection publique au monde qui permette à la fois une traversée de toute l’œuvre peinte, sculptée, gravée et dessinée de Picasso, comme l’évocation précise – à travers esquisses, études, croquis, carnets de dessins, états successifs de gravures, photographies, livres illustrés, films et documents – du processus créateur de l’artiste. Installé depuis 1985 dans l''Hôtel Salé, l’un des plus emblématiques des hôtels particuliers construits à la fin du XVIIe siècle dans le Marais, le musée vient de bénéficier d’une importante rénovation qui a permis de doubler les espaces d’exposition et de créer des espaces d’accueil et de services adaptés aux publics.', 'https://lh5.googleusercontent.com/p/AF1QipN-qy14KlJk_R2iJ00y4eFsRwJ6p18kpPAFxlc_=w408-h240-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(44, 'Place du Trocadero', 'Place du Trocadéro, 75016 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJz35dQONv5kcRtSVtSJLDOsA',  2.2887075, 48.8619569, 'Paris', 'S''il y a bien un jardin à visiter lorsque vous passez par la tour Eiffel, ce sont bien les Jardins du Trocadéro ! Créées lors de l''exposition spécialisée de 1937, ces 93 930m² d''espaces verts offrent une vue imprenable sur la tour Eiffel dominant la capitale (sortir son appareil photo est donc ici indispensable). Le centre du jardin est marqué par la célèbre fontaine de Varsovie pourvue de ses 20 canons à eau offrant un spectacle aquatique remarquable, notamment les nuits d''été où un éclairage particulier met en valeur les abords du bassin. Quelques sculptures couronnent le tout : chevaux dorées, tête de taureau, "L''Homme" de Traverse et la "La Femme" de Bacqué.', 'https://lh5.googleusercontent.com/p/AF1QipOnsqAT7ri1QugJ4ZAbUNoE3ufn3S8vmBtStzkP=w408-h306-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(45, 'Place Vendôme', 'Place Vendôme, 75001 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJuSTG7jFu5kcRQ64LSBxPIEE',  2.3294325, 48.86747099999999, 'Paris', 'Louis XIV voulait un lieu grandiose pour incarner le pouvoir absolu au cœur de Paris. Et Napoléon ne s’y trompa pas en remplaçant la statue du monarque, déboulonnée en 1792, par une colonne coulée dans le bronze de 1 200 canons arrachés à l’ennemi. Mais depuis le second Empire, la place, joyau octogonal de l’urbanisme classique, a changé de passion. Le luxe a supplanté la politique, et les grands noms de la joaillerie ont fait de l’écrin Vendôme et de la rue de la Paix qui la prolonge une coulée de diamants, de rubis et d’émeraudes.', 'https://lh5.googleusercontent.com/p/AF1QipMLuvkXfvC37n4JnAH7A3PzhRiw8iNtzXVEiw-W=w408-h229-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(47, 'Quai Branly Museum', '37 Quai Branly, 75007 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1900"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1100"
               }
            }
         ]', 'ChIJY8922uBv5kcRJESw9l2dlLc',  2.297894, 48.8608889, 'Paris', 'Le quai Branly est situé sur la Rive gauche de la Seine, entre les ponts de l''Alma et Bir-Hakeim. On y trouve de nombreux embarcadères d''où partent les croisières sur la Seine, le musée du Quai Branly, la maison de la Culture du Japon et le monument-symbole de toute la France, la tour Eiffel.', 'https://lh5.googleusercontent.com/p/AF1QipMkha4Q39vbdZWrn2jgGVo5vRWchRX6t0VOJ4ZI=w408-h272-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(48, 'Musée Rodin', '77 Rue de Varenne, 75007 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1745"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1745"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1745"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1745"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1745"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1745"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJQ9vMHipw5kcRWHC2ESjYaGQ',  2.3158354, 48.8553072, 'Paris', 'Derrière une façade toute en sobriété se cache un bijou d’hôtel particulier du XVIIIe siècle, qui abrite les collections permanentes du musée Rodin. Outre les chefs-d’œuvre du sculpteur, sont exposées quelques oeuvres de sa maîtresse et égérie au destin tourmenté, Camille Claudel. En flânant dans le vaste jardin, le visiteur découvre d’autres créations majeures du maître : Le Penseur, Les Bourgeois de Calais ou encore Les Portes de l’Enfer.', 'https://lh5.googleusercontent.com/p/AF1QipMiXMlxpKdc5nA9fmq6niVO1xTQVhcMc3jAm-3a=w408-h272-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(50, 'Sainte-Chapelle', '8 Boulevard du Palais, 75001 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0930"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0930"
               }
            }
         ]', 'ChIJR3122B9u5kcRaCck3PlB9DM',  2.344987, 48.85542, 'Paris', 'La Sainte-Chapelle fut construite par saint Louis au milieu du XIIIe siècle au cœur du Palais de la Cité pour abriter les reliques de la passion du Christ. Avec son ensemble unique de quinze hautes verrières, la Sainte-Chapelle constitue un ensemble exceptionnel de l’architecture gothique rayonnante.', 'https://lh4.googleusercontent.com/proxy/mDam0OCAzSNi4pXfygNRJaXvjFbAi2lfPKiJe3R0lxaghavqMZnjGUSo3xBvQyl9v4-Kyl7eqBlRooZdMLxsBopwVVxo2mE7Sz4xGI4mKE5tJfwvtxKOXtebvI71C2iQbEBM8kMNKAIKJSRp9WDf6UV8oOnwTQ=w408-h272-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(55, 'Musée de l''Armée', '129 Rue de Grenelle, 75007 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1000"
               }
            }
         ]', 'ChIJv-rX7Ndv5kcRJ0IdTc55HnY',  2.3125934, 48.85579329999999, 'Paris', 'Dans le cadre prestigieux de l''Hôtel national des Invalides naît en 1905 le musée de l''Armée, issu de la fusion du musée d''Artillerie (1796) et du musée historique de l''Armée créé cent ans plus tard, à la suite de l''exposition universelle. Le musée conserve sur 8 000 m² (musée, deux églises) 500 000 objets répertoriés. Ces données font du musée de l''armée le plus important musée d''histoire militaire de France et l''un des tout premiers au monde. Les collections permanentes du musée sont réparties en collections dites "historiques", correspondant à un circuit de présentation chronologique depuis l''Antiquité jusqu''à la fin de la seconde guerre mondiale, enrichi d''objets appartenant à des ensembles "thématiques" (emblèmes, peintures, décorations...). Ces collections thématiques sont donc soit présentées au fil des salles historiques, soit regroupées dans des espaces spécifiques. L’église du Dôme (plan de Jules Hardouin-Mansart) avec le lanternon ajouré culminant à 107 m, la grande fresque peinte sous la coupole par Charles de La Fosse et son dôme redoré en 1989 à l''occasion du bicentenaire de la Révolution Française pour la cinquième fois depuis sa création (12 kg d''or ont été nécessaires pour cette opération). Véritable panthéon militaire avec les tombeaux abritant le cœur de Vauban, la dépouille de Turenne et le cœur de la Tour d''Auvergne ; le Dôme lui accueille principalement le tombeau de Napoléon Ier, les sépultures de ses frères Joseph et Jérôme Bonaparte, de son fils, le roi de Rome, des généraux (Bertrand et Duroc) et des maréchaux (Foch et Lyautey).', 'https://lh5.googleusercontent.com/p/AF1QipPEHv_M_maJG5r2nAaNXsNataFsNKi-tVe99UZU=w408-h271-k-no', 1);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(56, 'Le Centre Pompidou', 'Place Georges-Pompidou, 75004 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 0,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1100"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 6,
                  "time" : "1100"
               }
            }
         ]', 'ChIJoyC4CRxu5kcRRTPcWX5srLc',  2.352245, 48.860642, 'Paris', 'Le Centre Pompidou, créé par Renzo Piano et Richard Rogers, est une merveille d’architecture du XXe siècle, reconnaissable à ses escalators extérieurs et à ses énormes tuyaux colorés. Il abrite le musée national d’Art moderne, référence mondiale pour ses collections d’art des XXe et XXIe siècles. Les œuvres d’artistes incontournables sont réparties chronologiquement, sur deux espaces : la période moderne de 1905 à 1960 (Matisse, Picasso, Dubuffet…) et la période contemporaine de 1960 à nos jours (Andy Warhol, Niki de Saint Phalle, Anish Kapoor…). En plus de ces collections permanentes, des expositions de renommée internationale sont organisées toute l’année au dernier étage, d’où la vue panoramique sur le centre de Paris et ses toits est imprenable. On y passe volontiers une demi-journée, voire la journée entière : on se restaure au Georges, on s’instruit à la Bibliothèque publique d’information et on flâne dans les rayons de la boutique. Au pied du Centre, sur la Piazza, l’atelier Brancusi présente une collection unique des œuvres de cet artiste majeur de l’histoire de la sculpture moderne.', 'https://lh5.googleusercontent.com/p/AF1QipMJQ3Y4fATy-Ym2SsfluslxVtfGaQNPQgBSV_pz=w408-h272-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(58, 'Les jardins du Trocadéro', 'Place du Trocadéro et du 11 Novembre, 75016 Paris, France', '[
            {
               "open" : {
                  "day" : 0,
                  "time" : "0000"
               }
            }
         ]', 'ChIJoxY3R-Nv5kcRWsRDGBoNIOk',  2.2892823, 48.8615963, 'Paris', 'S''il y a bien un jardin à visiter lorsque vous passez par la tour Eiffel, ce sont bien les Jardins du Trocadéro ! Créées lors de l''exposition spécialisée de 1937, ces 93 930m² d''espaces verts offrent une vue imprenable sur la tour Eiffel dominant la capitale (sortir son appareil photo est donc ici indispensable). Le centre du jardin est marqué par la célèbre fontaine de Varsovie pourvue de ses 20 canons à eau offrant un spectacle aquatique remarquable, notamment les nuits d''été où un éclairage particulier met en valeur les abords du bassin. Quelques sculptures couronnent le tout : chevaux dorées, tête de taureau, "L''Homme" de Traverse et la "La Femme" de Bacqué.', 'https://lh5.googleusercontent.com/p/AF1QipNVu1Lva7DWaFrRxinQsg6ShlUmUy-vRFMANZ0O=w408-h271-k-no', 2);
INSERT INTO public.server_places(id, name, address,  opening, place_id,  longitude, latitude, city, description, photo, duration) VALUES(59, 'Jardin des Tuileries', '113 Rue de Rivoli, 75001 Paris, France', '[
            {
               "close" : {
                  "day" : 0,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 0,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 1,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 1,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 2,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 3,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 4,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 5,
                  "time" : "0700"
               }
            },
            {
               "close" : {
                  "day" : 6,
                  "time" : "2100"
               },
               "open" : {
                  "day" : 6,
                  "time" : "0700"
               }
            }
         ]', 'ChIJAQAAMCxu5kcRx--_4QnbGcI',  2.3274943, 48.8634916, 'Paris', 'Le Jardin des Tuileries tient son nom des fabriques de tuiles qui se tenaient à l’endroit où la reine Catherine de Médicis a fait édifier le palais des Tuileries en 1564, aujourd’hui disparu. Le célèbre jardinier du roi, André Le Nôtre, lui donne à partir de 1664 son aspect actuel de jardin à la française. Le jardin, qui sépare le musée du Louvre de la place de la Concorde, est un lieu de promenade et de culture pour parisiens et touristes où les statues de Maillol côtoient celles de Rodin ou de Giacometti. Les deux bassins sont propices à la détente. Le musée de l’Orangerie dans lequel les visiteurs admirent des œuvres de Monet est installé au sud-ouest des Tuileries. De mars à décembre, des visites gratuites uniquement en français sont organisées. Et pour les amateurs de barbes à papa, de manèges et de sensations fortes, rendez-vous à la Fête des Tuileries, de juin à août.', 'https://lh4.googleusercontent.com/proxy/9zG-yD4gd7MOL-DRwpOsVD9xD7WWdrqOVZURcooMPKiFXix_R_l2rrnVBReIF8ADkM0SY8VM1knTET7FPauqTB5cGvcdPgrR_jv5rL1M1QNmR2iXzD4ekRB6ouDJ2OOmYtDUTtkkZSr2_Yz2N04KFYgljGi7qB8=w408-h306-k-no', 2);
