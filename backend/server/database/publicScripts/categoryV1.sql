INSERT INTO public.server_category(id, name) VALUES (0, 'établissement');
INSERT INTO public.server_category(id, name) VALUES (1, 'parc');
INSERT INTO public.server_category(id, name) VALUES (3, 'église');
INSERT INTO public.server_category(id, name) VALUES (4, 'lieu de culte');
INSERT INTO public.server_category(id, name) VALUES (5, 'musée');
INSERT INTO public.server_category(id, name) VALUES (6, 'monument');
INSERT INTO public.server_category(id, name) VALUES (9, 'mairie');
INSERT INTO public.server_category(id, name) VALUES (10, 'place');