# NEO4J

IN ORDER TO ADD EVERYTHING LOCALLY, RUN THE FOLLOWING :
- Add every csv file into import folder of database
- Open browser
- Run LOAD CSV WITH HEADERS FROM "file:///placesV1.csv" AS csvLine CREATE (p:Place {id: toInt(csvLine.id), name: csvLine.name})
- Run LOAD CSV WITH HEADERS FROM "file:///categoryV1.csv" AS csvLine CREATE (p:Category {id: toInt(csvLine.category_id), name: csvLine.name})
- Run LOAD CSV WITH HEADERS FROM "file:///relationPlaceCategoryV1.csv" AS csvLine MATCH(place:Place),(cate:Category) WHERE place.id = toInt(csvLine.from) AND cate.id = toInt(csvLine.to) CREATE (place)-[r:IS_CATEGORY]-(cate) RETURN r
- Run LOAD CSV WITH HEADERS FROM "file:///distanceV1.csv" AS csvLine MATCH(place1:Place),(place2:Place) WHERE place1.id = toInt(csvLine.from) AND place2.id = toInt(csvLine.to) CREATE (place1)-[r:DISTANCE { value: csvLine.value }]->(place2) RETURN r
