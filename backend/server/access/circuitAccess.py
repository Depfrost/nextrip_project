from server import models


def GetAllCircuit():
    return models.Circuit.objects.all()


def GetCircuitByID(id):
    return models.Circuit.objects.filter(id=id).first()

def GetCircuitByUserID(user_id):
    return models.Circuit_Users.objects.filter(user_id=user_id)
