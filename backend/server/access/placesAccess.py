from server import models

def GetAllPlaces():
    return models.Places.objects.all()

def GetPlaceByID(id):
    return models.Places.objects.filter(id=id).first()

def GetPlaceByCategoryId(id):
    return models.Category_Places.objects.filter(category_id=id)