from server import models

def GetAllUser():
    return models.User.objects.all()

def GetUserByID(id):
    return models.User.objects.filter(id=id).first()

