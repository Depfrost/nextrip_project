from server import models


def GetAll():
    return models.Category.objects.all()

def GetCategoryByID(id):
    return models.Category.objects.filter(id=id).first()