from server import models

def GetPlacesCategoryByPlaceID(id):
    return models.Category_Places.objects.filter(place_id=id)

def GetPlacesCategoryByCategoryID(id):
    return models.Category_Places.objects.filter(category_id=id)