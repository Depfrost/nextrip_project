import sys

from django.http import JsonResponse
from neo4j.v1 import GraphDatabase, basic_auth
from rest_framework import generics, serializers, mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, BasePermission
import json

from server.models import User, Category


class BasicUserSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = [
            'pk',
            'url',
            'username',
            'first_name',
            'last_name',
            'password',
            'email',
            'photo',
            'category',
            'photoaccess',
            'is_superuser',
            'is_staff',
            'is_active',
            'date_joined'
        ]
        read_only_fields = ('is_superuser', 'is_staff', 'is_active', 'date_joined')

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        new_json = {}
        if "category" in validated_data:
            old_json = json.loads(validated_data['category'])
            for elem in old_json:
                name = elem['name']
                rate = elem['value']
                new_json[name] = rate
            user.category = new_json
            # Insert in Neo4j database
            if 'test' not in sys.argv:
                driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                              auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
                session = driver.session()
                pk = str(user.pk)
                query = "CREATE (n:Profil { id: " + str(pk) + "," \
                         "établissement: " + str(new_json["établissement"]) + ", " \
                         "musée: " + str(new_json["musée"]) + ", " \
                        "mairie: " + str(new_json["mairie"]) + ", " \
                        "parc: " + str(new_json["parc"]) + ", " \
                        "église: " + str(new_json["église"]) + ", " \
                        "lieu_de_culte: " + str(new_json["lieu de culte"]) + ", " \
                        "monument: " + str(new_json["monument"]) + ", " \
                        "place: " + str(new_json["place"]) + " }) return n "
                session.run(query)
        else:
            validated_data['category'] = "{}"
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, user, validated_data):
        for attr, value in validated_data.items():
            setattr(user, attr, value)
        new_json = {}
        if "category" in validated_data:
            old_json = json.loads(validated_data['category'])
            for elem in old_json:
                name = elem['name']
                rate = elem['value']
                new_json[name] = rate
            user.category = new_json
        else:
            validated_data['category'] = "{}"
        user.set_password(validated_data['password'])
        user.save()
        # Insert in Neo4j database
        if 'test' not in sys.argv:
            driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                         auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
            session = driver.session()
            pk = str(user.pk)
            query = "MATCH (n:Profil) WHERE n.id =" + str(pk) + \
                    " SET établissement: " + str(new_json["établissement"]) + ", " \
                    "musée: " + str(new_json["musée"]) + ", " \
                    "mairie: " + str(new_json["mairie"]) + ", " \
                    "parc: " + str(new_json["parc"]) + ", " \
                    "église: " + str(new_json["église"]) + ", " \
                    "lieu_de_culte: " + str(new_json["lieu de culte"]) + ", " \
                    "monument: " + str(new_json["monument"]) + ", " \
                    "place: " + str(new_json["place"]) + " }) return n "
            session.run(query)
        return user

    def get_url(self, obj):
        request = self.context.get("request")
        return obj.get_api_url(request=request)

    def validate_email(self, value):
        qs = User.objects.filter(email__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exists():
            raise serializers.ValidationError("This email is already used by another user.")
        return value


class FullUserSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = [
            'pk',
            'url',
            'username',
            'first_name',
            'last_name',
            'password',
            'email',
            'photo',
            'category',
            'photoaccess',
            'is_superuser',
            'is_staff',
            'is_active',
            'date_joined'
        ]

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        new_json = {}
        if "category" in validated_data:
            old_json = json.loads(validated_data['category'])
            for elem in old_json:
                name = elem['name']
                rate = elem['value']
                new_json[name] = rate
            if 'test' not in sys.argv:
                driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                              auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
                session = driver.session()
                pk = str(user.pk)
                query = "CREATE (n:Profil { id: " + str(pk) + "," \
                         "établissement: " + str(new_json["établissement"]) + ", " \
                         "musée: " + str(new_json["musée"]) + ", " \
                        "mairie: " + str(new_json["mairie"]) + ", " \
                        "parc: " + str(new_json["parc"]) + ", " \
                        "église: " + str(new_json["église"]) + ", " \
                        "lieu_de_culte: " + str(new_json["lieu de culte"]) + ", " \
                        "monument: " + str(new_json["monument"]) + ", " \
                        "place: " + str(new_json["place"]) + " }) return n "
                session.run(query)
        user.category = new_json
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, user, validated_data):
        for attr, value in validated_data.items():
            setattr(user, attr, value)
        new_json = {}
        if "category" in validated_data:
            old_json = json.loads(validated_data['category'])
            for elem in old_json:
                name = elem['name']
                rate = elem['value']
                new_json[name] = rate
        user.category = new_json
        user.set_password(validated_data['password'])
        user.save()
        return user

    def get_url(self, obj):
        request = self.context.get("request")
        return obj.get_api_url(request=request)

    def validate_email(self, value):
        qs = User.objects.filter(email__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exists():
            raise serializers.ValidationError("This email is already used by another user.")
        return value


class UserAPIViewPermission(BasePermission):
    message = "Permission denied."
    my_safe_methods = ['POST']

    def has_permission(self, request, view):
        if request.method in self.my_safe_methods:
            return True
        else:
            if request.user.is_superuser:
                return True
            else:
                return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return obj
        else:
            return obj == request.user


class UserAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    """
    get:
    Return a list of all existing users

    post:
    Create a new user with specified parameters
    """
    pass
    lookup_field = 'pk'
    model = User
    permission_classes = [
        UserAPIViewPermission,
    ]

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return FullUserSerializer
        return BasicUserSerializer

    def get_queryset(self):
        return User.objects.all()

    def post(self, request, *args, **kwargs):
        self.serializer_class = BasicUserSerializer
        return self.create(request, *args, **kwargs)

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}


class UserRudViewPermission(BasePermission):
    message = "Permission denied."
    my_safe_methods = ['DELETE', 'GET', 'PUT', 'PATCH']

    def has_permission(self, request, view):
        if request.method in self.my_safe_methods:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return obj
        else:
            return obj == request.user


class UserRudView(generics.RetrieveUpdateDestroyAPIView):
    """
    delete:
    Delete the specified user

    get:
    Return the specified user

    put:
    Update the specified user with given parameters

    patch:
    Update the specified user with given parameters
    """
    pass
    lookup_field = 'pk'
    model = User
    permission_classes = [
        UserRudViewPermission
    ]

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return FullUserSerializer
        return BasicUserSerializer

    def get_queryset(self):
        return User.objects.all()

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def getAuthenticatedUser(req):
    """
    Return the user associated with the given token
    """
    uid = req.user
    if uid != "AnonymousUser":
        profil = json.loads(uid.category)
        profil_to_json = []
        if profil != {}:
            for category in Category.objects.all():
                profil_to_json.append(dict(
                    id=category.id,
                    name=category.name,
                    value=profil[category.name]
                ))
        to_json = dict(
            id=uid.id,
            username=uid.username,
            first_name=uid.first_name,
            last_name=uid.last_name,
            email=uid.email,
            photo=uid.photo,
            category=profil_to_json,
            photoaccess=uid.photoaccess,
            is_superuser=uid.is_superuser,
            is_staff=uid.is_staff,
            is_active=uid.is_active,
            date_joined=uid.date_joined,
        )
        return JsonResponse(to_json)
    else:
        return JsonResponse({
            'token': "No user is associated with this token"
        }, status=404)
