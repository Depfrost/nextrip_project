import datetime
import sys

from django.http import JsonResponse
from rest_framework import generics, serializers
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from server import models
import json
from neo4j.v1 import GraphDatabase, basic_auth

from math import pow, sqrt

from server.converters.placesConverter import placesConverterToJson
from server.utils.api.googleApi import getTimeBeforeArrivedPlace
from server.utils.api.weatherApi import getDailyWeatherHumidity


class BasicCircuitSerializer(serializers.ModelSerializer):
    """user_id = serializers.IntegerField(many=True)
    list = serializers.JSONField(True)"""

    user_id = serializers.IntegerField(write_only=True)
    list = serializers.CharField(write_only=True)

    class Meta:
        model = models.Circuit
        fields = [
            'pk',
            'name',
            'city',
            'user_id',
            'list',
            'rating'
        ]

    def create(self, validated_data):
        # Create circuit
        if 'name' in validated_data:
            name = validated_data['name']
        else:
            name = 'New Circuit'
        city_provided = self.data.get('city', None)
        if city_provided is None:
            return JsonResponse({
                'status': 'BAD REQUEST'
            }, status=400)
        city = validated_data['city']
        list_places = json.loads(validated_data['list'])
        photo = "https://people.xiph.org/~xiphmont/demo/ghost/no-data.png"
        if list_places is not None or len(list_places) != 0:
            try:
                place = models.Places.objects.get(id=list_places[0]['Place_id'])
                photo = place.photo
            except models.Places.DoesNotExist:
                pass
        new_circuit = models.Circuit(name=name, city=city, photo=photo)
        new_circuit.save()
        # Create circuit user
        user_id = validated_data['user_id']
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            if user.id != user_id and not user.is_superuser:
                new_circuit.delete()
                raise PermissionError
        user_obj = models.User.objects.get(id=user_id)
        new_user_circuit = models.Circuit_Users(circuit=new_circuit, user=user_obj)
        # Create circuit place
        new_user_circuit.save()
        for record in list_places:
            place_id = int(record['Place_id'])
            place_count = models.Places.objects.filter(id=place_id).count()
            if place_count == 0:
                new_circuit.delete()
                new_user_circuit.delete()
                raise ValueError
            place_obj = models.Places.objects.get(id=place_id)
            new_place_circuit = models.Circuit_Places(circuit=new_circuit, place=place_obj)
            new_place_circuit.save()
        # Create node circuit
        driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                      auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
        session = driver.session()
        query = 'CREATE (n:Circuit {' \
            'id: ' + str(new_circuit.id) + ',' \
            'name: "' + name + '",' \
            'city: "Paris"' \
            '}) return n'
        session.run(query)
        return new_circuit

    def get_url(self, obj):
        request = self.context.get("request")
        return obj.get_api_url(request=request)


class CreateCircuitView(generics.CreateAPIView):
    """
       post:
       Create a new circuit with specified parameters
    """
    pass
    lookup_field = 'pk'
    model = models.Circuit
    permission_classes = [
        IsAuthenticated,
    ]

    def get_serializer_class(self):
        return BasicCircuitSerializer

    def get_queryset(self):
        return models.Circuit.objects.all()

    def post(self, request, *args, **kwargs):
        self.serializer_class = BasicCircuitSerializer
        try:
            return self.create(request, *args, **kwargs)
        except PermissionError:
            return JsonResponse({
                'status': 'FORBIDDEN'
            }, status=403)
        except ValueError:
            return JsonResponse({
                'status': 'BAD REQUEST'
            }, status=400)

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}


class RatesCircuitSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Circuits_Marks
        fields = [
            'pk',
            'circuit',
            'user',
            'marks'
        ]

    def create(self, validated_data):
        circuit = validated_data['circuit']
        user = validated_data['user']
        marks = validated_data['marks']
        if marks != -1 and (marks < 0 or marks > 10):
            raise ValueError
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user_req = request.user
            if user_req.id != user.id and not user_req.is_superuser:
                raise PermissionError
        # Raise an ObjectDoesNotExist exception if the place is not found in database
        circuit_obj = models.Circuit.objects.get(id=circuit.id)
        marks_count = models.Circuits_Marks.objects.filter(circuit=circuit, user=user).count()
        if marks_count == 0:
            mark_obj = models.Circuits_Marks(circuit=circuit, user=user, marks=marks)
        else:
            mark_obj = models.Circuits_Marks.objects.get(circuit=circuit, user=user)
            mark_obj.marks = marks
        mark_obj.save()

        circuit_marks_objs = models.Circuits_Marks.objects.filter(circuit=circuit)
        sum = 0
        for circuit_marks_obj in circuit_marks_objs:
            sum += circuit_marks_obj.marks
        circuit_obj.rating = sum / circuit_marks_objs.count()
        circuit_obj.save()

        circuit_places_objs = models.Circuit_Places.objects.filter(circuit=circuit.id)
        sum = 0
        rated_places_counter = 0
        # Compute the average mark for places in the circuit
        for circuit_place_obj in circuit_places_objs:
            place = models.Places.objects.filter(pk=circuit_place_obj.place_id).first()
            if place.rating != -1:
                sum += place.rating
                rated_places_counter += 1
        if rated_places_counter != 0:
            places_mark = sum / rated_places_counter
        else:
            places_mark = -1
        circuit_mark = circuit_obj.rating

        # Get the average of both marks
        nb_mark = 2
        if places_mark == -1:
            nb_mark -= 1
            places_mark = 0
        final_mark = (circuit_mark + places_mark) / nb_mark

        if 'test' not in sys.argv:
            # Update Neo4j database
            driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                          auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
            session = driver.session()
            query = 'MATCH (a:Profil{id: ' + str(user.id) + '}),(b:Circuit{id: ' + str(circuit.id) + '})' \
                    'CREATE UNIQUE (a)-[r:Visited]->(b)' \
                    'SET r.value =' + str(final_mark)
            session.run(query)
        return mark_obj

    def get_url(self, obj):
        request = self.context.get("request")
        return obj.get_api_url(request=request)


def is_place_open(opening_hours, date):
    opening_hours = json.loads(opening_hours)
    for period in opening_hours:
        if period["open"]["day"] == 0 and period["open"]["time"] == "0000":
            return True
        if str(period["open"]["day"]) == date.strftime("%w"):
            if period["open"]["time"] <= date.strftime("%H%M"):
                if period["close"] is None or period["close"]["time"] > date.strftime("%H%M"):
                    return True
    return False


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetCircuitMarkByUser(request, user_id, circuit_id):
    """
    Return the mark of a circuit by user, use user_id 0 to get the average mark of all users for the circuit
    """
    try:
        if request and hasattr(request, "user"):
            user_req = request.user
            if user_req.id != int(user_id) and not user_req.is_superuser:
                raise PermissionError

        circuit_places_objs = models.Circuit_Places.objects.filter(circuit=circuit_id)
        sum = 0
        rated_places_counter = 0
        # Average for all user
        if int(user_id) == 0:
            # Compute the average mark for places in the circuit
            for circuit_place_obj in circuit_places_objs:
                place = models.Places.objects.filter(pk=circuit_place_obj.place_id).first()
                if place.rating != -1:
                    sum += place.rating
                    rated_places_counter += 1
            # Get the mark for the circuit
            circuit_mark = models.Circuit.objects.filter(pk=circuit_id).first().rating
        # Mark for a particular user
        else:
            # Compute the average mark for places in the circuit
            for circuit_place_obj in circuit_places_objs:
                place_mark_obj = models.Places_Marks.objects.filter(place=circuit_place_obj.place_id, user=user_id).first()
                if place_mark_obj is not None:
                    sum += place_mark_obj.marks
                    rated_places_counter += 1
            # Get the mark for the circuit
            circuit_mark_obj = models.Circuits_Marks.objects.filter(circuit=circuit_id, user=user_id).first()
            if circuit_mark_obj is not None:
                circuit_mark = circuit_mark_obj.marks
            else:
                circuit_mark = -1

        if rated_places_counter != 0:
            places_mark = sum / rated_places_counter
        else:
            places_mark = -1

        # Get the average of both marks
        nb_mark = 2
        if places_mark == -1:
            nb_mark -= 1
            places_mark = 0
        if circuit_mark == -1:
            nb_mark -= 1
            circuit_mark = 0
        if nb_mark != 0:
            final_mark = (circuit_mark + places_mark) / nb_mark
        else:
            final_mark = -1

        if 'test' not in sys.argv:
            # Update Neo4j database
            driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                          auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
            session = driver.session()
            query = 'MATCH (a:Profil{id: ' + str(user_id) + '}),(b:Circuit{id: ' + str(circuit_id) + '})' \
                    'CREATE UNIQUE (a)-[r:Visited]->(b)' \
                    'SET r.value =' + str(final_mark)
            session.run(query)
        to_json = dict(
            mark=final_mark
        )

        return JsonResponse(to_json, safe=False)
    except PermissionError:
        return JsonResponse({
            'status': 'FORBIDDEN'
        }, status=403)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetGenerateCircuit(request, city, longitude, latitude, duration):
    """
    Generate a recommended circuit
    """
    try:
        """
            1] Position -> prendre les lieux les plus proches
            2] Faire une évaluation de chaque lieu
                b] Prendre la météo en compte
                a] Prendre en compte le profil 
            3] Ajouter le temps passé etc...
            4] Vérifier que le temps est respecté
            5] Relancer avec la position du lieu
        """
        profil = json.loads(models.User.objects.filter(id=request.user.id).first().category)
        compteur = 0
        compteur_while = 0
        time = 0.0
        list_place = list()
        distance = 1500
        """
            GET DAILY WEATHER
        """
        #humidity = getDailyWeatherHumidity()
        """
            RECHERCHE DE LA DATABASE
        """
        # FIXME duration check must be done at the end (need to keep searching till no more place close enought to fit in allowed time)
        while compteur < 10 and time < float(duration) and compteur_while < 20:
            compteur_while += 1
            max_node_value = -1
            selected_node = None
            query = 'select * from server_places where ST_DWithin(ST_MakePoint(longitude, latitude)::geography, ST_MakePoint(' + longitude + ',' + latitude + ')::geography, ' + str(distance) + ')'
            """
                VIRER LES PLACES DEJA AJOUTEES
            """
            for chosen_place in list_place:
                query += ' AND id<>' + str(chosen_place.id)
            query = query + ';'
            """
                RECUPERER LES PLACES A BONNE DISTANCE
            """
            available_places = models.Places.objects.raw(query)
            for element in available_places:
                list_category_id = models.Category_Places.objects.filter(place_id=element.id)
                list_category = list()
                for category in list_category_id:
                    list_category.append(models.Category.objects.filter(id=category.category_id).first().name)
                node_value = 0
                """
                    EVALUER LA PLACE EN QUESTION
                """
                max_cat = list_category[0]
                for category in list_category:
                    if profil[category] > profil[max_cat]:
                        max_cat = category
                node_value += profil[max_cat]
                """
                if humidity > 80 and list_category.__contains__('parc'):
                    node_value -= 3
                if humidity < 30 and list_category.__contains__('parc'):
                    node_value += 3
                """
                """
                    SELECTIONNER LE MAX
                """
                if node_value > max_node_value:
                    max_node_value = node_value
                    selected_node = element
            if selected_node is not None:
                origins = str(latitude) + "," + str(longitude)
                destinations = str(selected_node.latitude) + "," + str(selected_node.longitude)
                json_resp = getTimeBeforeArrivedPlace(origins, destinations)
                deltatime = selected_node.duration + (json_resp['rows'][0]['elements'][0]['duration']['value'])/3600
                """
                if is_place_open(selected_node.opening, datetime.datetime.now() + datetime.timedelta(hours=deltatime)):
                    list_place.append(selected_node)
                    time += deltatime
                    longitude = str(selected_node.longitude)
                    latitude = str(selected_node.latitude)
                    distance = 1500
                    compteur += 1
                else:
                    distance = distance * 5
                """
                list_place.append(selected_node)
                time += deltatime
                longitude = str(selected_node.longitude)
                latitude = str(selected_node.latitude)
                distance = 1500
                compteur += 1
            else:
                distance = distance * 2
        # FIXME PARCOURS DU PLUS COURT CHEMIN -> TODO
        to_json = []
        for record in list_place:
            to_json.append(placesConverterToJson(record))
        return JsonResponse(to_json, safe=False)
    except Exception as ex:
        print(ex)
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetCircuitByCity(request, city, user_id):
    """
    Return the recommended circuits by city and user
    """
    if str(request.user.id) != user_id and not request.user.is_superuser:
        return JsonResponse({
            'status': 'FORBIDDEN REQUEST'
        }, status=403)
    try:
        profilUser = json.loads(models.User.objects.filter(id=user_id).first().category)

        driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24  786",
                                      auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
        session = driver.session()

        query = 'MATCH (n:Profil) RETURN n.id AS id, n.établissement AS etablissement, n.musée AS musee, ' \
                'n.mairie as mairie, n.parc as parc,  n.église AS église, n.lieu_de_culte as culte, n.monument as monument, n.place as place'

        queryProfil = 'MATCH (n:Profil) WHERE  n.établissement = $etablissement AND n.musée = $musee ' \
                'AND n.mairie = $mairie AND n.parc  =  $parc AND n.église= $église AND n.lieu_de_culte = $culte AND n.monument = $monument AND n.place = $place' \
                ' RETURN n.id AS id, n.établissement AS etablissement, n.musée AS musee, ' \
                'n.mairie as mairie, n.parc as parc,  n.église AS église, n.lieu_de_culte as culte, n.monument as monument, n.place as place'

        createProfil = 'CREATE(n:Profil {établissement:$etablissement, musée:$musee ' \
                ',mairie:$mairie, parc:$parc, église:$église, n.lieu_de_culte:$culte, n.monument:$monument, n.place:$place' \
                ' RETURN n.id AS id, n.établissement AS etablissement, n.musée AS musee, ' \
                'n.mairie as mairie, n.parc as parc, n.église AS église, n.lieu_de_culte as culte, n.monument as monument, n.place as place'

        profilUser["établissement"] = profilUser['établissement'] if "établissement" in profilUser else 0
        profilUser['musée'] = profilUser['musée'] if "musée" in profilUser else 0
        profilUser['mairie'] = profilUser['mairie'] if "mairie" in profilUser else 0
        profilUser['parc'] = profilUser['parc'] if "parc" in profilUser else 0
        profilUser['église'] = profilUser['église'] if "église" in profilUser else 0
        profilUser['lieu de culte'] = profilUser['lieu de culte'] if "lieu de culte" in profilUser else 0
        profilUser['monument'] = profilUser['monument'] if "monument" in profilUser else 0
        profilUser['place'] = profilUser['place'] if "place" in profilUser else 0

        profil = session.run(queryProfil, etablissement=profilUser['établissement'], musee=profilUser['musée'], mairie=profilUser['mairie'], parc=profilUser['parc'],   église=profilUser['église'], culte=profilUser['lieu de culte'],
                             monument=profilUser['monument'], place=profilUser['place'])

        if profil is None:
            profil = session.run(createProfil, etablissement=profilUser['établissement'], musee=profilUser['musée'], mairie=profilUser['mairie'], parc=profilUser['parc'],   église=profilUser['église'], culte=profilUser['lieu de culte'],
                             monument=profilUser['monument'], place=profilUser['place'])
        result = session.run(query)
        circuits = []
        """
            Changer le coefficient ou utiliser une autre methode pour faire la diff
            Changer base de donnéees pour que les notes soient en float
        """
        for record in result:

            meanError = sqrt(pow(profilUser["établissement"] - record["etablissement"], 2) + pow(profilUser["musée"] -  record["musee"], 2) + \
                        pow(profilUser["mairie"] - record["mairie"], 2)+ \
                          pow(profilUser["parc"] -  record["parc"], 2) +  pow(profilUser["église"] - record["église"], 2)+ \
                        pow(profilUser["lieu de culte"] - record["culte"], 2) + pow(profilUser[
                              "monument"] - record["monument"], 2)+ pow(profilUser["place"] - record["place"], 2))

            if meanError != 0.0:
                obj = dict(id=record["id"], value=meanError)
                circuits.append(obj)

        circuits.sort(key=lambda x:x["value"])
        bestProfil = {"id": 1}
        if len(circuits) != 0:
            bestProfil = circuits[0]

        """
        Ameliorer le parcours du graphe
        """
        recommendedCircuits = session.run(
            'MATCH (p:Profil)-[r:Visited]->(c:Circuit) WHERE p.id = ' + str(bestProfil["id"]) + ' RETURN c.id AS id ORDER BY r.value')

        listId = []
        for record in recommendedCircuits:
            listId.append(record["id"])

        to_json = []
        session.close()
        """
        Rechercher si circuit deja visité
        """
        for record in listId:

            circuitRecord = models.Circuit.objects.filter(id=str(record)).first()
            if circuitRecord is not None:
                obj = dict(
                    id=circuitRecord.id,
                    name=circuitRecord.name,
                    photo=circuitRecord.photo
                )
                to_json.append(obj)
        return JsonResponse(to_json, safe=False)

    except Exception as e:
        return JsonResponse({
            'status': e
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetCircuitById(request, id):
    """
    Return the details of a circuit
    """
    try:
        res = models.Circuit.objects.filter(id=id).first()
        if res == None:
            return JsonResponse({
                'status': 'BAD REQUEST'
            }, status=400)
        placeRes = models.Circuit_Places.objects.filter(circuit_id=res.id)
        to_json = []
        for record in placeRes:
            place_record = models.Places.objects.filter(id=record.place_id).first()
            if place_record != None:
                obj = dict(
                    id=str(place_record.id),
                    name=place_record.name,
                    address=place_record.address,
                    longitude=str(place_record.longitude),
                    latitude=str(place_record.latitude),
                    city=place_record.city,
                    description=place_record.description,
                    opening=place_record.opening,
                    photo=place_record.photo
                )
                to_json.append(obj)
        return JsonResponse(to_json, safe=False)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetCircuitByUser(request, user_id):
    """
    Return the circuits done by an user
    """
    if str(request.user.id) != user_id and not request.user.is_superuser:
        return JsonResponse({
            'status': 'FORBIDDEN REQUEST'
        }, status=403)
    try:
        user = models.User.objects.filter(id=user_id).first()
        if user == None:
            return JsonResponse({
                'status': 'BAD REQUEST'
            }, status=400)
        res = models.Circuit_Users.objects.filter(user_id=user_id)
        to_json = []
        for record in res:
            circuit_record = models.Circuit.objects.filter(id=record.circuit_id).first()
            obj = dict(
                id=circuit_record.id,
                name=circuit_record.name,
                photo=circuit_record.photo
            )
            to_json.append(obj)
        return JsonResponse(to_json, safe=False)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GenerateKmean(request, k_number):

    try:
        """""
        driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                      auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))
        """


        driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                      auth=basic_auth("test", "b.tTOLhehGBESB.DuTb08POjbY1RbLC"))
        session = driver.session()

        kmean = int(k_number) + 1

        query = 'MATCH (n:Profil) WITH n, rand() as r ORDER BY r RETURN n.id AS id, n.établissement AS etablissement, ' \
                'n.musée AS musee, n.mairie as mairie, n.parc as parc, n.église AS église, n.lieu_de_culte as culte, ' \
                'n.monument as monument, n.place as place LIMIT  ' + str(k_number)

        session.run('MATCH (c:Centroid) DETACH DELETE c')

        result = session.run(query)
        iterator = 1
        queryInit = 'MERGE (:Centroid {établissement: $etablissement, musée: $musee, mairie: $mairie, parc: $parc, ' \
                    'lieu_de_culte: $culte, église: $église, monument: $monument, place: $place, id: $id, iteration: 1})'

        for record in result:

            centroid = session.run(queryInit, etablissement=record['etablissement'], musee=record['musee'],
                                 mairie=record['mairie'], parc=record['parc'], église=record['église'],
                                 culte=record['culte'],
                                 monument=record['monument'], place=record['place'], id=iterator)
            iterator += 1

        setRequest = ''
        testQuery = 'MATCH (s:Profil), '
        testCase = ''

        for x in range(1, kmean):

            testQuery += '(c' + str(x) + ':Centroid{id:' + str(x) + ', iteration: $iter}) '
            setRequest += 'SET s.distC' + str(
                x) + '= (s.établissement - c' + str(x) + '.établissement)^2 + (s.musée - c' + str(
                x) + '.musée)^2 + (s.mairie - c' + str(x) + '.mairie)^2 + (s.parc - c' + str(x) + '.parc)^2 + ' \
                                                                                                 '(s.lieu_de_culte - c' + str(
                x) + '.lieu_de_culte)^2 + (s.église - c' + str(x) + '.église)^2 + (s.monument - c' + str(
                x) + '.monument)^2 + (s.place - c' + str(x) + '.place)^2 '

            if x != kmean - 1:
                testQuery += ', '
                testCase += ' when '

                for y in range(1, kmean):
                    if y != x:
                        if y != kmean - 1:
                            testCase += ' s.distC' + str(x) + '<= s.distC' + str(y) + ' and '
                        else:
                            testCase += ' s.distC' + str(x) + '<= s.distC' + str(y) + ' '

                testCase += ' then c' + str(x)
            else:
                testCase += ' else c' + str(x) + ' end as minC CREATE (s)-[:IN_CLUSTER]->(minC) RETURN *'

        testQuery += setRequest
        testQuery += ' WITH s, case '
        testQuery += testCase

        for iteration in range(1, kmean):
            cluster = session.run(testQuery, iter=iteration)
            y = iteration + 1

            for x in range(1, kmean):
                testCentroid = 'MATCH (s1:Profil)-[:IN_CLUSTER]->(c1:Centroid {id:' +str(x)+', iteration: $iter}) WITH avg(s1.établissement) ' \
                            'as s1établissement, avg(s1.musée) as s1musée, avg(s1.mairie) as s1mairie, avg(s1.parc) as s1parc, ' \
                            'avg(s1.lieu_de_culte) as s1lieu_de_culte, avg(s1.église) as s1église, avg(s1.monument) as s1monument, ' \
                            'avg(s1.place) as s1place' \
                           ' CREATE (:Centroid {établissement: s1établissement, musée: s1musée, mairie: s1mairie, parc: s1parc, ' \
                           'lieu_de_culte: s1lieu_de_culte, église: s1église, monument: s1monument, place: s1place, id:'+str(x) +', ' \
                           'iteration: $iter2}) '
                centroid = session.run(testCentroid, iter=iteration, iter2=y)

        return JsonResponse({
            'status': ' OK'
        }, status=200)


    except Exception as e:
        return JsonResponse({
            'status': e
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def AssignCluster(request, profil_id):

    try:
        driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                      auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))

        session = driver.session()

        queryCluster = 'MATCH (s:Profil{id: $id}), (c1:Centroid{id: 1, iteration: $iter}), (c2:Centroid{id: 2, iteration: $iter}), (c3:Centroid{id: 3, iteration: $iter}) ' \
                       'SET s.distC1 = (s.établissement - c1.établissement)^2 + (s.musée - c1.musée)^2 + (s.mairie - c1.mairie)^2 + (s.parc - c1.parc)^2 + ' \
                       '(s.lieu_de_culte - c1.lieu_de_culte)^2 + (s.église - c1.église)^2 + (s.monument - c1.monument)^2 + (s.place - c1.place)^2 ' \
                       'SET s.distC2 = (s.établissement - c2.établissement)^2 + (s.musée - c2.musée)^2 + (s.mairie - c2.mairie)^2 + (s.parc - c2.parc)^2 + ' \
                       '(s.lieu_de_culte - c2.lieu_de_culte)^2 + (s.église - c2.église)^2 + (s.monument - c2.monument)^2 + (s.place - c2.place)^2 ' \
                       'SET s.distC3 = (s.établissement - c3.établissement)^2 + (s.musée - c3.musée)^2 + (s.mairie - c3.mairie)^2 + (s.parc - c3.parc)^2' \
                       ' + (s.lieu_de_culte - c3.lieu_de_culte)^2 + (s.église - c3.église)^2 + (s.monument - c3.monument)^2 + (s.place - c3.place)^2 ' \
                       'WITH s, case when s.distC1 <= s.distC2 and s.distC1 <= s.distC3 then c1 when s.distC2 <= s.distC1 and s.distC2 <= s.distC3 then c2 ' \
                       'else c3 end as minC CREATE (s)-[:IN_CLUSTER]->(minC) RETURN *'

        idProfile= int(profil_id)
        cluster = session.run(queryCluster, id=idProfile, iter=3)

        return JsonResponse({
            'status': ' OK'
        }, status=200)

    except Exception as e:
        return JsonResponse({
            'status': e
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetKmean(request, city, user_id):
    """
        Return the recommended circuits by city and user
        """
    if str(request.user.id) != user_id and not request.user.is_superuser:
        return JsonResponse({
            'status': 'FORBIDDEN REQUEST'
        }, status=403)
    try:
        profilUser = json.loads(models.User.objects.filter(id=user_id).first().category)

        driver = GraphDatabase.driver("bolt://hobby-eijemgoegikmgbkeafcjhibl.dbs.graphenedb.com:24786",
                                      auth=basic_auth("nextrip", "b.PKYCYDP28SOe.XUZBANlwEczS0YUR"))


        session = driver.session()

        query = 'MATCH (p:Profil)-[:IN_CLUSTER]->(c:Centroid) WHERE  p.établissement = $etablissement AND p.musée = $musee ' \
                ' AND p.mairie = $mairie AND p.parc  =  $parc AND p.église= $église AND p.lieu_de_culte = $culte AND p.monument = $monument ' \
                'AND p.place = $place AND c.iteration = 3 ' \
                'WITH c  MATCH (n:Profil)-[:IN_CLUSTER]->(c) WITH n MATCH (n)-[:Visited]->() RETURN n.id AS id, n.établissement AS etablissement, n.musée AS musee, ' \
                'n.mairie as mairie, n.parc as parc,  n.église AS église, n.lieu_de_culte as culte, n.monument as monument, n.place as place'

        queryProfil = 'MATCH (n:Profil) WHERE  n.établissement = $etablissement AND n.musée = $musee ' \
                      'AND n.mairie = $mairie AND n.parc  =  $parc AND n.église= $église AND n.lieu_de_culte = $culte AND n.monument = $monument AND n.place = $place' \
                      ' RETURN n.id as id'

        createProfil = 'CREATE(n:Profil {établissement:$etablissement, musée:$musee ' \
                       ',mairie:$mairie, parc:$parc, église:$église, n.lieu_de_culte:$culte, n.monument:$monument, n.place:$place' \
                       ' RETURN n.id AS id, n.établissement AS etablissement, n.musée AS musee, ' \
                       'n.mairie as mairie, n.parc as parc, n.église AS église, n.lieu_de_culte as culte, n.monument as monument, n.place as place'

        profilUser["établissement"] = profilUser['établissement'] if "établissement" in profilUser else 0
        profilUser['musée'] = profilUser['musée'] if "musée" in profilUser else 0
        profilUser['mairie'] = profilUser['mairie'] if "mairie" in profilUser else 0
        profilUser['parc'] = profilUser['parc'] if "parc" in profilUser else 0
        profilUser['église'] = profilUser['église'] if "église" in profilUser else 0
        profilUser['lieu de culte'] = profilUser['lieu de culte'] if "lieu de culte" in profilUser else 0
        profilUser['monument'] = profilUser['monument'] if "monument" in profilUser else 0
        profilUser['place'] = profilUser['place'] if "place" in profilUser else 0

        """
        profil = session.run(queryProfil, etablissement=profilUser['établissement'], musee=profilUser['musée'],
                             mairie=profilUser['mairie'], parc=profilUser['parc'], église=profilUser['église'],
                             culte=profilUser['lieu de culte'],
                             monument=profilUser['monument'], place=profilUser['place'])
    
        if profil is None:
            profil = session.run(createProfil, etablissement=profilUser['établissement'], musee=profilUser['musée'],
                                 mairie=profilUser['mairie'], parc=profilUser['parc'], église=profilUser['église'],
                                 culte=profilUser['lieu de culte'],
                                 monument=profilUser['monument'], place=profilUser['place'])

        """
        result = session.run(query,etablissement=profilUser['établissement'], musee=profilUser['musée'],
                             mairie=profilUser['mairie'], parc=profilUser['parc'], église=profilUser['église'],
                             culte=profilUser['lieu de culte'],
                             monument=profilUser['monument'], place=profilUser['place']
                             )

        circuits = []
        """
            Changer le coefficient ou utiliser une autre methode pour faire la diff
            Changer base de donnéees pour que les notes soient en float
        """
        for record in result:

            meanError = sqrt(pow(profilUser["établissement"] - record["etablissement"], 2) + pow(
                profilUser["musée"] - record["musee"], 2) + \
                             pow(profilUser["mairie"] - record["mairie"], 2) + \
                             pow(profilUser["parc"] - record["parc"], 2) + pow(profilUser["église"] - record["église"],
                                                                               2) + \
                             pow(profilUser["lieu de culte"] - record["culte"], 2) + pow(profilUser[
                                                                                             "monument"] - record[
                                                                                             "monument"], 2) + pow(
                profilUser["place"] - record["place"], 2))

            if meanError != 0.0:
                obj = dict(id=record["id"], value=meanError)
                circuits.append(obj)

        circuits.sort(key=lambda x: x["value"])
        bestProfil = {"id": 1}
        if len(circuits) != 0:
            bestProfil = circuits[0]
        """
        Ameliorer le parcours du graphe
        """
        recommendedCircuits = session.run(
            'MATCH (p:Profil)-[r:Visited]->(c:Circuit) WHERE p.id = ' + str(
                bestProfil["id"]) + ' RETURN c.id AS id ORDER BY r.value')

        listId = []
        for record in recommendedCircuits:
            listId.append(record["id"])

        to_json = []
        session.close()
        """
        Rechercher si circuit deja visité
        """
        for record in listId:

            circuitRecord = models.Circuit.objects.filter(id=str(record)).first()
            if circuitRecord is not None:
                obj = dict(
                    id=circuitRecord.id,
                    name=circuitRecord.name,
                    photo=circuitRecord.photo
                )
                to_json.append(obj)
        return JsonResponse(to_json, safe=False)

    except Exception as e:
        return JsonResponse({
            'status': e
        }, status=400)


class PostCircuitRateView(generics.CreateAPIView):
    """
       post:
       Post a new note for the circuit with specified parameters
    """
    pass
    lookup_field = 'pk'
    model = models.Circuits_Marks
    permission_classes = [
        IsAuthenticated,
    ]

    def get_serializer_class(self):
        return RatesCircuitSerializer

    def get_queryset(self):
        return models.Places_Marks.objects.all()

    def post(self, request, *args, **kwargs):
        self.serializer_class = RatesCircuitSerializer
        try:
            return self.create(request, *args, **kwargs)
        except PermissionError:
            return JsonResponse({
                'status': 'FORBIDDEN'
            }, status=403)
        except ValueError:
            return JsonResponse({
                'status': 'BAD REQUEST mark value must be between 0 and 10'
            }, status=400)

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}
