from django.http import JsonResponse
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, serializers
import json

from server import models
from server.access.categoryAccess import GetCategoryByID
from server.access.categoryPlacesAccess import GetPlacesCategoryByCategoryID
from server.access.placesAccess import GetPlaceByID
from server.access.placesMarksAccess import GetPlacesMarksByPlaceIDAndUserID
from server.converters.placesConverter import placesConverterToJson


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetPlaceById(request, pk):
    """
    Return the details of a place
    """
    try:
        record = GetPlaceByID(pk)
        if record is not None:
            to_json = placesConverterToJson(record)
            return JsonResponse(to_json, safe=False)
        else:
            return JsonResponse({
                'status': 'BAD REQUEST'
            }, status=400)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetPlacesByCategoryId(request, city, category_id):
    """
    Return places by category
    """
    try:
        isCategoryExist = GetCategoryByID(category_id)
        if isCategoryExist is None:
            return JsonResponse({
                'status': 'BAD REQUEST'
            }, status=400)
        else:
            res = GetPlacesCategoryByCategoryID(category_id)
            to_json = []
            for list_record in res:
                record = GetPlaceByID(list_record.place_id)
                if record is not None:
                    if record.city == city:
                        obj = placesConverterToJson(record)
                        to_json.append(obj)
            return JsonResponse(to_json, safe=False)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetPlaceMarkByUser(request, user_id, place_id):
    """
    Return the mark of a place by user, use user_id 0 to get the average mark of all users for the place
    """
    try:
        if request and hasattr(request, "user"):
            user_req = request.user
            if user_req.id != int(user_id) and not user_req.is_superuser:
                raise PermissionError
        place = GetPlaceByID(place_id)
        if int(user_id) == 0:
            mark = place.rating
        else:
            place_mark_obj = GetPlacesMarksByPlaceIDAndUserID(place_id, user_id)
            if place_mark_obj is None:
                mark = -1
            else:
                mark = place_mark_obj.marks
        to_json = dict(
            mark=mark
        )
        return JsonResponse(to_json, safe=False)
    except PermissionError:
        return JsonResponse({
            'status': 'FORBIDDEN'
        }, status=403)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)


class RatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Places_Marks
        fields = [
            'pk',
            'place',
            'user',
            'marks'
        ]

    def create(self, validated_data):
        place = validated_data['place']
        user = validated_data['user']
        marks = validated_data['marks']
        if marks < 0 or marks > 10:
            raise ValueError
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user_req = request.user
            if user_req.id != user.id and not user_req.is_superuser:
                raise PermissionError
        # Save the mark into table
        marks_count = models.Places_Marks.objects.filter(place=place, user=user).count()
        if marks_count == 0:
            mark_obj = models.Places_Marks(place=place, user=user, marks=marks)
            mark_obj.save()
        else:
            mark_obj = models.Places_Marks.objects.get(place=place, user=user)
            mark_obj.marks = marks
            mark_obj.save()

        # Update the average of marks for this place
        # Raise an ObjectDoesNotExist exception if the place is not found in database
        place_mark_objs = models.Places_Marks.objects.filter(place=place)
        sum = 0
        for mark in place_mark_objs:
            sum += mark.marks
        if place_mark_objs.count() != 0:
            mark = sum / place_mark_objs.count()
        else:
            mark = -1
        place_obj = models.Places.objects.filter(pk=place.id).first()
        place_obj.rating = mark
        place_obj.save()

        # Update the profil
        user_obj = models.User.objects.get(id=user.id)
        user_categories = json.loads(user_obj.category)
        categories = models.Category_Places.objects.filter(place_id=place)
        # For each category update the profil
        for category in categories:
            category_name = models.Category.objects.get(id=category.category_id)
            user_categories[str(category_name.name)] -= (user_categories[str(category_name.name)] - marks) / 5
        user_obj.category = json.dumps(user_categories, ensure_ascii=False)
        user_obj.save()
        return mark_obj

    def get_url(self, obj):
        request = self.context.get("request")
        return obj.get_api_url(request=request)


class PostRateView(generics.CreateAPIView):
    """
       post:
       Post a new note for a place with specified parameters. Also update the user profile
    """
    pass
    lookup_field = 'pk'
    model = models.Places_Marks
    permission_classes = [
        IsAuthenticated,
    ]

    def get_serializer_class(self):
        return RatesSerializer

    def get_queryset(self):
        return models.Places_Marks.objects.all()

    def post(self, request, *args, **kwargs):
        self.serializer_class = RatesSerializer
        try:
            return self.create(request, *args, **kwargs)
        except PermissionError:
            return JsonResponse({
                'status': 'FORBIDDEN'
            }, status=403)
        except ValueError:
            return JsonResponse({
                'status': 'BAD REQUEST mark value must be between 0 and 10'
            }, status=400)

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}
