from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from server.access.categoryAccess import GetCategoryByID, GetAll
from server.access.categoryPlacesAccess import GetPlacesCategoryByPlaceID
from server.converters.categoryConverter import categoryConverterToJson


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetAllCategory(request):
    """
    Return all the available categories
    """
    try:
        allCategory = GetAll()
        to_json = []
        for record in allCategory:
            to_json.append(categoryConverterToJson(record))
        return JsonResponse(to_json, safe=False)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetCategoryById(request, pk):
    """
    Return the corresponding category
    """
    try:
        obj = GetCategoryByID(pk)
        to_json = categoryConverterToJson(obj)
        return JsonResponse(to_json, safe=False)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def GetCategoryByPlace(request, place_id):
    """
    Return the corresponding category for place
    """
    try:
        res = GetPlacesCategoryByPlaceID(place_id)
        to_json = []
        for record in res:
            category_record = GetCategoryByID(record.category_id)
            to_json.append(categoryConverterToJson(category_record))
        return JsonResponse(to_json, safe=False)
    except:
        return JsonResponse({
            'status': 'BAD REQUEST'
        }, status=400)
