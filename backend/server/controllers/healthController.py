from django.http import JsonResponse
from rest_framework.decorators import api_view

@api_view(['GET'])
def healthController(request):
    """
    Check if the server is alive
    """
    return JsonResponse({
        'status': 'alive'
    })
