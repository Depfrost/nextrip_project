"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url

from server.controllers import usersController, placeController, categoryController, circuitController, healthController
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer
from rest_framework_jwt.views import obtain_jwt_token

schema_view = get_schema_view(title='Nextrip API', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])

urlpatterns = [

    url(r'^users$', usersController.UserAPIView.as_view(), name='user-listcreate'),
    url(r'^users/(?P<pk>\d+)$', usersController.UserRudView.as_view(), name='user-rud'),
    url(r'^user$', usersController.getAuthenticatedUser, name='user-get'),

    url(r'^place/(?P<pk>\d+)$', placeController.GetPlaceById, name='place-by-id'),
    url(r'place/rate$',placeController.PostRateView.as_view(), name='rate-place'),
    url(r'place/rate/(?P<user_id>\d+)/(?P<place_id>\d+)$', placeController.GetPlaceMarkByUser, name='get-rate-by-user'),
    url(r'^places/(?P<city>[\w\-]+)/(?P<category_id>\d+)$', placeController.GetPlacesByCategoryId, name='place-by-city-categoryid'),

    url('categories', categoryController.GetAllCategory, name='categories'),
    url(r'^category/(?P<pk>\d+)$', categoryController.GetCategoryById, name='category-by-id'),
    url(r'^category/place/(?P<place_id>\d+)$', categoryController.GetCategoryByPlace, name='category-by-place'),

    url(r'^doc/$', schema_view, name='docs'),
    url(r'^auth/login$', obtain_jwt_token, name='api-login'),

    url(r'circuit/rate$', circuitController.PostCircuitRateView.as_view(), name='rate-circuit'),
    url(r'circuit/rate/(?P<user_id>\d+)/(?P<circuit_id>\d+)$', circuitController.GetCircuitMarkByUser, name='get-circuit-rate'),
    url(r'^circuits/city/(?P<city>[\w\-]+)/(?P<user_id>\d+)$', circuitController.GetKmean, name='circuit-by-place-userid'),
    url(r'^circuit/(?P<id>\d+)$', circuitController.GetCircuitById, name='circuit-by-id'),
    url(r'circuit$', circuitController.CreateCircuitView.as_view(), name='circuit-create'),
    url(r'^circuits/user/(?P<user_id>\d+)$', circuitController.GetCircuitByUser, name='circuit-by-userid'),
    url(r'^circuits/recommendation/city/(?P<city>[\w\-]+)/(?P<user_id>\d+)$', circuitController.GetKmean, name='circuit-kmean'),
    url(r'^circuits/cluster/(?P<profil_id>\d+)$', circuitController.AssignCluster, name='circuit-cluster'),
    url(r'^circuits/kmean/(?P<k_number>\d+)$', circuitController.GenerateKmean, name='circuit-centroid'),
    url(r'^circuit/(?P<city>[\w\-]+)/(?P<longitude>\d+\.\d+)/(?P<latitude>\d+\.\d+)/(?P<duration>\d+\.\d+)$', circuitController.GetGenerateCircuit, name='generate-circuit'),
    url('health', healthController.healthController, name='server-health')
]
